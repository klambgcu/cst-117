﻿// Corrected by Kelly Lamb
// Exercise 5-9


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//Lydia's code - find the errors!
namespace CST117_IC08_console
{
    class Set
    {
        private List<int> elements;


        public Set()
        {
            elements = new List<int>();
        }

        public bool addElement(int val)
        {
            if (containsElement(val)) return false;
            else
            {
                elements.Add(val);
                return true;
            }
        }

        private bool containsElement(int val)
        {
            for (int i = 0; i < elements.Count; i++)
            {
                if (val == elements[i])
                    return true;
                // Remove this else return false - exits prematurely
                // only return false after inspecting all elements
                //else
                //    return false;
            }
            return false;
        }

        public override string ToString()
        {
            string str = "";
            foreach (int i in elements)
            {
                str += i + " ";
            }
            return str;
        }

        public void clearSet()
        {
            elements.Clear();
        }

        public Set union(Set rhs)
        {
            // Create a temporary Set to hold union
            Set results = new Set();

            // Initialize it with a copy of this set (lhs)
            results.elements = new List<int>(this.elements);

            for (int i = 0; i < rhs.elements.Count; i++)
            {
                // Do not add to this (lhs), add to copy instead
                // this.addElement(rhs.elements[i]);
                results.addElement(rhs.elements[i]);
            }
            // Returning the wrong set
            // Should return a copy of this with rhs added to it
            // so that no change occurs to this (lhs).
            // return rhs;
            return results;
        }
    }
}

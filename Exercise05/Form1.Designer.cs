/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 3-5
 * ---------------------------------------------------------------
 * Description:
 * Create a form that accepts a number of terms to approximate PI
 * Must use an appropriate loop
 * Formula: 4 - 4/3 + 4/5 - 4/7 + 4/9...
 * Include pertinent icon and form title
 * 
 */


namespace Exercise05
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelEnterTerm = new System.Windows.Forms.Label();
            this.textBoxNumOfTerms = new System.Windows.Forms.TextBox();
            this.buttonCalculate = new System.Windows.Forms.Button();
            this.labelTermCount = new System.Windows.Forms.Label();
            this.labelValueOfPI = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelEnterTerm
            // 
            this.labelEnterTerm.AutoSize = true;
            this.labelEnterTerm.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEnterTerm.Location = new System.Drawing.Point(25, 27);
            this.labelEnterTerm.Name = "labelEnterTerm";
            this.labelEnterTerm.Size = new System.Drawing.Size(146, 24);
            this.labelEnterTerm.TabIndex = 0;
            this.labelEnterTerm.Text = "Enter # of terms:";
            // 
            // textBoxNumOfTerms
            // 
            this.textBoxNumOfTerms.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumOfTerms.Location = new System.Drawing.Point(205, 24);
            this.textBoxNumOfTerms.Name = "textBoxNumOfTerms";
            this.textBoxNumOfTerms.Size = new System.Drawing.Size(100, 29);
            this.textBoxNumOfTerms.TabIndex = 1;
            // 
            // buttonCalculate
            // 
            this.buttonCalculate.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonCalculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculate.Location = new System.Drawing.Point(25, 90);
            this.buttonCalculate.Name = "buttonCalculate";
            this.buttonCalculate.Size = new System.Drawing.Size(142, 50);
            this.buttonCalculate.TabIndex = 2;
            this.buttonCalculate.Text = "CALCULATE";
            this.buttonCalculate.UseVisualStyleBackColor = false;
            this.buttonCalculate.Click += new System.EventHandler(this.buttonCalculate_Click);
            // 
            // labelTermCount
            // 
            this.labelTermCount.AutoSize = true;
            this.labelTermCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTermCount.Location = new System.Drawing.Point(25, 179);
            this.labelTermCount.Name = "labelTermCount";
            this.labelTermCount.Size = new System.Drawing.Size(313, 24);
            this.labelTermCount.TabIndex = 3;
            this.labelTermCount.Text = "Approximate value of pi after 0 terms";
            // 
            // labelValueOfPI
            // 
            this.labelValueOfPI.AutoSize = true;
            this.labelValueOfPI.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelValueOfPI.Location = new System.Drawing.Point(25, 242);
            this.labelValueOfPI.Name = "labelValueOfPI";
            this.labelValueOfPI.Size = new System.Drawing.Size(21, 24);
            this.labelValueOfPI.TabIndex = 4;
            this.labelValueOfPI.Text = "=";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 290);
            this.Controls.Add(this.labelValueOfPI);
            this.Controls.Add(this.labelTermCount);
            this.Controls.Add(this.buttonCalculate);
            this.Controls.Add(this.textBoxNumOfTerms);
            this.Controls.Add(this.labelEnterTerm);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Approximate PI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelEnterTerm;
        private System.Windows.Forms.TextBox textBoxNumOfTerms;
        private System.Windows.Forms.Button buttonCalculate;
        private System.Windows.Forms.Label labelTermCount;
        private System.Windows.Forms.Label labelValueOfPI;
    }
}


/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 3-5
 * ---------------------------------------------------------------
 * Description:
 * Create a form that accepts a number of terms to approximate PI
 * Must use an appropriate loop
 * Formula: 4 - 4/3 + 4/5 - 4/7 + 4/9...
 * Include pertinent icon and form title
 * 
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise05
{
    public partial class Form1 : Form
    {

        public void showSumTwoIntegers(int num1, int num2) { }

        // 2.
        public double computeAverageFiveDoubles(double num1, double num2, double num3, double num4, double num5) { return 0.00; }

        // 3.
        public int computeSumTwoRandomIntegers() { return 1; }

        // 4.
        public bool isSumEvenlyDivisibleByThree(int num1, int num2, int num3) { return true; }

        // 5.
        public void showShortestString(string str1, string str2) { }

        // 6.
        public double findLargestDoubleInArray(double[] arrayDoubles) { return 0.00; }

        // 7.
        public int[] createArrayFiftyIntegers() { return new int[50]; }

        // 8.
        public bool isBooleanEqual(bool value1, bool value2) { return true; }

        // 9.
        public double computerProduct(int num1, double num2) { return 0.00; }

        // 10.
        public double computerAverage(int[,] intArray) { return 0.00; }





        public Form1()
        {
            InitializeComponent();
        }

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            //
            // Calculate the approximation of PI using a loop
            // Formula: 4 - 4/3 + 4/5 - 4/7 + 4/9 ....
            // 
            int termEntry;
            int loop;
            double numerator = 4.0;
            double denominator = 1.0;
            double pi = 0.0;

            // Accept & Parse term entry - calculate on success, error message of failure
            bool success = int.TryParse(textBoxNumOfTerms.Text, out termEntry);
            if (success)
            {
                // Use a for-loop to count number of iterations based upon user input (termEntry)
                for (loop = 0; loop < termEntry; loop++)
                {
                    // Calc pi based upon formula above
                    pi += (numerator / denominator);

                    //Ttoggle sign instead of checking on whether to add or subtract
                    numerator = -numerator;
                    
                    // Increment denom by two - keep it odd
                    denominator += 2.0;
                }

                // Display count message and approximation
                labelTermCount.Text = "Approximate value of pi after " + loop.ToString() + " terms";
                labelValueOfPI.Text = "= " + pi.ToString();
            }
            else
            {
                // Parsing failed on term entry - show a message
                MessageBox.Show("Number of Terms should be positive whole number", "Invalid Entry For Number of Terms", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}

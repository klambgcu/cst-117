﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-18
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 6-4
 * ---------------------------------------------------------------
 * Description:
 * Create application to simulate Tic-Tac-Toe
 */

namespace Project04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelBoard00 = new System.Windows.Forms.Label();
            this.labelBoard21 = new System.Windows.Forms.Label();
            this.labelBoard20 = new System.Windows.Forms.Label();
            this.labelBoard12 = new System.Windows.Forms.Label();
            this.labelBoard11 = new System.Windows.Forms.Label();
            this.labelBoard10 = new System.Windows.Forms.Label();
            this.labelBoard02 = new System.Windows.Forms.Label();
            this.labelBoard01 = new System.Windows.Forms.Label();
            this.labelBoard22 = new System.Windows.Forms.Label();
            this.labelResults = new System.Windows.Forms.Label();
            this.buttonNewGame = new System.Windows.Forms.Button();
            this.buttonExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelBoard00
            // 
            this.labelBoard00.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard00.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard00.Location = new System.Drawing.Point(25, 24);
            this.labelBoard00.Name = "labelBoard00";
            this.labelBoard00.Size = new System.Drawing.Size(75, 75);
            this.labelBoard00.TabIndex = 0;
            this.labelBoard00.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard21
            // 
            this.labelBoard21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard21.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard21.Location = new System.Drawing.Point(129, 227);
            this.labelBoard21.Name = "labelBoard21";
            this.labelBoard21.Size = new System.Drawing.Size(75, 75);
            this.labelBoard21.TabIndex = 1;
            this.labelBoard21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard20
            // 
            this.labelBoard20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard20.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard20.Location = new System.Drawing.Point(25, 227);
            this.labelBoard20.Name = "labelBoard20";
            this.labelBoard20.Size = new System.Drawing.Size(75, 75);
            this.labelBoard20.TabIndex = 2;
            this.labelBoard20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard12
            // 
            this.labelBoard12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard12.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard12.Location = new System.Drawing.Point(233, 124);
            this.labelBoard12.Name = "labelBoard12";
            this.labelBoard12.Size = new System.Drawing.Size(75, 75);
            this.labelBoard12.TabIndex = 3;
            this.labelBoard12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard11
            // 
            this.labelBoard11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard11.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard11.Location = new System.Drawing.Point(129, 124);
            this.labelBoard11.Name = "labelBoard11";
            this.labelBoard11.Size = new System.Drawing.Size(75, 75);
            this.labelBoard11.TabIndex = 4;
            this.labelBoard11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard10
            // 
            this.labelBoard10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard10.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard10.Location = new System.Drawing.Point(25, 124);
            this.labelBoard10.Name = "labelBoard10";
            this.labelBoard10.Size = new System.Drawing.Size(75, 75);
            this.labelBoard10.TabIndex = 5;
            this.labelBoard10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard02
            // 
            this.labelBoard02.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard02.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard02.Location = new System.Drawing.Point(233, 24);
            this.labelBoard02.Name = "labelBoard02";
            this.labelBoard02.Size = new System.Drawing.Size(75, 75);
            this.labelBoard02.TabIndex = 6;
            this.labelBoard02.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard01
            // 
            this.labelBoard01.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard01.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard01.Location = new System.Drawing.Point(129, 24);
            this.labelBoard01.Name = "labelBoard01";
            this.labelBoard01.Size = new System.Drawing.Size(75, 75);
            this.labelBoard01.TabIndex = 7;
            this.labelBoard01.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelBoard22
            // 
            this.labelBoard22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelBoard22.Font = new System.Drawing.Font("Microsoft Sans Serif", 50F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)), true);
            this.labelBoard22.Location = new System.Drawing.Point(233, 227);
            this.labelBoard22.Name = "labelBoard22";
            this.labelBoard22.Size = new System.Drawing.Size(75, 75);
            this.labelBoard22.TabIndex = 8;
            this.labelBoard22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelResults
            // 
            this.labelResults.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResults.Location = new System.Drawing.Point(25, 325);
            this.labelResults.Name = "labelResults";
            this.labelResults.Size = new System.Drawing.Size(283, 34);
            this.labelResults.TabIndex = 9;
            this.labelResults.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonNewGame
            // 
            this.buttonNewGame.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonNewGame.Location = new System.Drawing.Point(25, 376);
            this.buttonNewGame.Name = "buttonNewGame";
            this.buttonNewGame.Size = new System.Drawing.Size(124, 39);
            this.buttonNewGame.TabIndex = 10;
            this.buttonNewGame.Text = "New Game";
            this.buttonNewGame.UseVisualStyleBackColor = true;
            this.buttonNewGame.Click += new System.EventHandler(this.buttonNewGame_Click);
            // 
            // buttonExit
            // 
            this.buttonExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExit.Location = new System.Drawing.Point(184, 376);
            this.buttonExit.Name = "buttonExit";
            this.buttonExit.Size = new System.Drawing.Size(124, 39);
            this.buttonExit.TabIndex = 11;
            this.buttonExit.Text = "Exit";
            this.buttonExit.UseVisualStyleBackColor = true;
            this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 442);
            this.Controls.Add(this.buttonExit);
            this.Controls.Add(this.buttonNewGame);
            this.Controls.Add(this.labelResults);
            this.Controls.Add(this.labelBoard22);
            this.Controls.Add(this.labelBoard01);
            this.Controls.Add(this.labelBoard02);
            this.Controls.Add(this.labelBoard10);
            this.Controls.Add(this.labelBoard11);
            this.Controls.Add(this.labelBoard12);
            this.Controls.Add(this.labelBoard20);
            this.Controls.Add(this.labelBoard21);
            this.Controls.Add(this.labelBoard00);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tic-Tac-Toe";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelBoard00;
        private System.Windows.Forms.Label labelBoard01;
        private System.Windows.Forms.Label labelBoard02;
        private System.Windows.Forms.Label labelBoard10;
        private System.Windows.Forms.Label labelBoard11;
        private System.Windows.Forms.Label labelBoard12;
        private System.Windows.Forms.Label labelBoard20;
        private System.Windows.Forms.Label labelBoard21;
        private System.Windows.Forms.Label labelBoard22;
        private System.Windows.Forms.Label labelResults;
        private System.Windows.Forms.Button buttonNewGame;
        private System.Windows.Forms.Button buttonExit;
    }
}


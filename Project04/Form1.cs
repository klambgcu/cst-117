﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-18
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 6-4
 * ---------------------------------------------------------------
 * Description:
 * Create application to simulate Tic-Tac-Toe
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

using OfficeOpenXml;

namespace Project04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();


            using (ExcelPackage excel = new ExcelPackage())
            {
                excel.Workbook.Worksheets.Add("Worksheet1");
                excel.Workbook.Worksheets.Add("Worksheet2");
                excel.Workbook.Worksheets.Add("Worksheet3");

                var headerRow = new List<string[]>() { new string[] { "ID", "First Name", "Last Name", "DOB" } };

                // Determine the header range (e.g. A1:D1)
                string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                // Target a worksheet
                var worksheet = excel.Workbook.Worksheets["Worksheet1"];

                // Popular header row data
                worksheet.Cells[headerRange].LoadFromArrays(headerRow);

                // Row Styling
                worksheet.Cells[headerRange].Style.Font.Bold = true;
                worksheet.Cells[headerRange].Style.Font.Size = 14;
                worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Blue);

                // Add Data to Specific Cell
                worksheet.Cells["A1"].Value = "Hello World!";

                // Add Data to Multiple Cells
                var cellData = new List<object[]>()
                {
                  new object[] {0, 0, 0, 1},
                  new object[] {10,7.32,7.42,1},
                  new object[] {20,5.01,5.24,1},
                  new object[] {30,3.97,4.16,1},
                  new object[] {40,3.97,4.16,1},
                  new object[] {50,3.97,4.16,1},
                  new object[] {60,3.97,4.16,1},
                  new object[] {70,3.97,4.16,1},
                  new object[] {80,3.97,4.16,1},
                  new object[] {90,3.97,4.16,1},
                  new object[] {100,3.97,4.16,1}
                };
                worksheet.Cells[2, 1].LoadFromArrays(cellData);

                // Save Excel file
                FileInfo excelFile = new FileInfo("test.xlsx");
                excel.SaveAs(excelFile);

                // Check if Excel is installed
                bool isExcelInstalled = Type.GetTypeFromProgID("Excel.Application") != null ? true : false;

                if (isExcelInstalled)
                {
                    // System call to start excel application loading with our new file
                    System.Diagnostics.Process.Start(excelFile.ToString());
                }

            }
        }

        // Define Game Constants
        private const int MAXROWS = 3;
        private const int MAXCOLS = 3;
        private const int PLAYERX = 1;
        private const int PLAYERO = -1;
        private const int EMPTY = 0;

        // Define Game Variables
        private int[,] gameBoard = { { EMPTY, EMPTY, EMPTY }, { EMPTY, EMPTY, EMPTY }, { EMPTY, EMPTY, EMPTY } };
        private int player = PLAYERX; // X = 1, O = -1;
        private Label[,] labelGameBoard = new Label[MAXROWS, MAXCOLS];
        private Random random;


        private void buttonExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void buttonNewGame_Click(object sender, EventArgs e)
        {
            // Clear Game Board and Game Board Display
            clearGameBoard();

            // Game Loop 1-9 Max
            for (int loop = 1; loop <= 9; loop++)
            {
                choosePlayPosition();
                drawGameBoard();
                if (loop > 4 && isGameOver())
                {
                    labelResults.Text = (player == PLAYERX) ? "X Wins!" : "O Wins!";
                    break;
                }
                else if (loop == 9)
                {
                    labelResults.Text = "Cats Game - No Winner.";
                }

                // Toggle between players X = 1, O = -1
                player = -player;
            }
        }

        private bool isGameOver()
        {
            // Check for win in the three rows
            if (gameBoard[0, 0] == player && gameBoard[0, 1] == player && gameBoard[0, 2] == player) return true;
            if (gameBoard[1, 0] == player && gameBoard[1, 1] == player && gameBoard[1, 2] == player) return true;
            if (gameBoard[2, 0] == player && gameBoard[2, 1] == player && gameBoard[2, 2] == player) return true;

            // Check for win in the three cols
            if (gameBoard[0, 0] == player && gameBoard[1, 0] == player && gameBoard[2, 0] == player) return true;
            if (gameBoard[0, 1] == player && gameBoard[1, 1] == player && gameBoard[2, 1] == player) return true;
            if (gameBoard[0, 2] == player && gameBoard[1, 2] == player && gameBoard[2, 2] == player) return true;

            // Check for win in the two diagonals
            if (gameBoard[0, 0] == player && gameBoard[1, 1] == player && gameBoard[2, 2] == player) return true;
            if (gameBoard[0, 2] == player && gameBoard[1, 1] == player && gameBoard[2, 0] == player) return true;

            // Reach here - not a winner yet - return false
            return false;
        }

        private void choosePlayPosition()
        {
            // Choose a random position to play
            // Repeat process if chosen position is not empty (0);
            int xPos = 0;
            int yPos = 0;

            do
            {
                xPos = random.Next(0, MAXCOLS);
                yPos = random.Next(0, MAXROWS);
            } while (gameBoard[yPos, xPos] != EMPTY);

            // Available position found - set it to the player
            gameBoard[yPos, xPos] = player;
        }

        private void clearGameBoard()
        {
            // Loop through arrays - set gameboard to empty and clear displays
            for (int rows = 0; rows < MAXROWS; rows++)
            {
                for (int cols = 0; cols < MAXCOLS; cols++)
                {
                    gameBoard[rows, cols] = EMPTY;
                    labelGameBoard[rows, cols].Text = "";
                }
            }
        }

        private void drawGameBoard()
        {
            // Loop through arrays - set gameboard display appropriately based upon game board values
            for (int rows = 0; rows < MAXROWS; rows++)
            {
                for (int cols = 0; cols < MAXCOLS; cols++)
                {
                    int play = gameBoard[rows, cols];
                    if (play == PLAYERO)
                        labelGameBoard[rows, cols].Text = "O";
                    else if (play == PLAYERX)
                        labelGameBoard[rows, cols].Text = "X";
                    else
                        labelGameBoard[rows, cols].Text = "";
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Set the random seed - this process seems to work quite well.
            random = new Random((int)Guid.NewGuid().GetHashCode());

            // Assign labels into two dimensional array to ease processing
            labelGameBoard[0, 0] = labelBoard00;
            labelGameBoard[0, 1] = labelBoard01;
            labelGameBoard[0, 2] = labelBoard02;
            labelGameBoard[1, 0] = labelBoard10;
            labelGameBoard[1, 1] = labelBoard11;
            labelGameBoard[1, 2] = labelBoard12;
            labelGameBoard[2, 0] = labelBoard20;
            labelGameBoard[2, 1] = labelBoard21;
            labelGameBoard[2, 2] = labelBoard22;

            // Prepare game board for play
            clearGameBoard();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            // exit application when form is closed
            Application.Exit();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project04
{
    public partial class SplashScreen : Form
    {
        public SplashScreen()
        {
            InitializeComponent();
        }

        // Use a timer class to show screen and delay application start 
        Timer timer;
        private void SplashScreen_Shown(object sender, EventArgs e)
        {
            timer = new Timer();
            
            // Set delay time interval to 5 sec
            timer.Interval = 5000;
            
            // Start the delay timer
            timer.Start();

            // Add event to handle timer ending
            timer.Tick += timer_Tick;
        }
        void timer_Tick(object sender, EventArgs e)
        {
            // Call Stop to end delay
            timer.Stop();

            // Display the Application Main Form
            Form1 form = new Form1();
            form.Show();

            // Hide this Splash Screen
            this.Hide();
        }
    }
}

﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-13
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 5-8
 * ---------------------------------------------------------------
 * Description:
 * Create application to solve problem 4 at end of chapter 6.
 * Calculate total calories consumed per day from user entry
 * for number of grams consumed in fat and carbohydrates.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise08
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}

﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-13
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 5-8
 * ---------------------------------------------------------------
 * Description:
 * Create application to solve problem 4 at end of chapter 6.
 * Calculate total calories consumed per day from user entry
 * for number of grams consumed in fat and carbohydrates.
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise08
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Compute and return amount of calories from fat grams consumed
        // Calories = Fat grams x 9
        private int FatCalories(int amountFatGrams)
        {
            return amountFatGrams * 9;
        }

        // Compute and return amount of calories from carbohydrates grams consumed
        // Calories = Carbs grams x 4
        private int CarbCalories(int amountCarbGrams)
        {
            return amountCarbGrams * 4;
        }

        private void buttonCalculateCalories_Click(object sender, EventArgs e)
        {
            // Define local variables
            int gramsFat = 0;
            int gramsCarbs = 0;
            int totalCalories = 0;

            // Get fat grams consumed from user input
            if (int.TryParse(textBoxFatGrams.Text, out gramsFat))
            {
                // Get carbohydrate grams consumed from user input
                if (int.TryParse(textBoxCarbGrams.Text, out gramsCarbs))
                {
                    // Confirm that entries are positive
                    if (gramsCarbs >= 0 && gramsFat >= 0)
                    {
                        // Calculate and display total number of calories consumed 
                        totalCalories = FatCalories(gramsFat) + CarbCalories(gramsCarbs);
                        textBoxTotalCalories.Text = totalCalories.ToString();
                    }
                    else
                    {
                        MessageBox.Show("Enter positive integers please");
                    }
                }
                else
                {
                    // Display error message for carbohydrate grams value
                    MessageBox.Show("Enter an integer for the carbohydrate grams consumed");
                }
            }
            else
            {
                // Display error message for fat grams value
                MessageBox.Show("Enter an integer for the fat grams consumed");
            }
        }
    }
}

﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-13
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 5-8
 * ---------------------------------------------------------------
 * Description:
 * Create application to solve problem 4 at end of chapter 6.
 * Calculate total calories consumed per day from user entry
 * for number of grams consumed in fat and carbohydrates.
 */

namespace Exercise08
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxGramsPerDay = new System.Windows.Forms.GroupBox();
            this.labelFatGrams = new System.Windows.Forms.Label();
            this.labelCarbGrams = new System.Windows.Forms.Label();
            this.textBoxFatGrams = new System.Windows.Forms.TextBox();
            this.textBoxCarbGrams = new System.Windows.Forms.TextBox();
            this.buttonCalculateCalories = new System.Windows.Forms.Button();
            this.labelTotalCalories = new System.Windows.Forms.Label();
            this.textBoxTotalCalories = new System.Windows.Forms.TextBox();
            this.groupBoxGramsPerDay.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxGramsPerDay
            // 
            this.groupBoxGramsPerDay.Controls.Add(this.textBoxCarbGrams);
            this.groupBoxGramsPerDay.Controls.Add(this.textBoxFatGrams);
            this.groupBoxGramsPerDay.Controls.Add(this.labelCarbGrams);
            this.groupBoxGramsPerDay.Controls.Add(this.labelFatGrams);
            this.groupBoxGramsPerDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxGramsPerDay.Location = new System.Drawing.Point(30, 27);
            this.groupBoxGramsPerDay.Name = "groupBoxGramsPerDay";
            this.groupBoxGramsPerDay.Size = new System.Drawing.Size(333, 157);
            this.groupBoxGramsPerDay.TabIndex = 0;
            this.groupBoxGramsPerDay.TabStop = false;
            this.groupBoxGramsPerDay.Text = "Grams Consumed Per Day";
            // 
            // labelFatGrams
            // 
            this.labelFatGrams.AutoSize = true;
            this.labelFatGrams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFatGrams.Location = new System.Drawing.Point(85, 52);
            this.labelFatGrams.Name = "labelFatGrams";
            this.labelFatGrams.Size = new System.Drawing.Size(107, 20);
            this.labelFatGrams.TabIndex = 0;
            this.labelFatGrams.Text = "Grams of Fat:";
            // 
            // labelCarbGrams
            // 
            this.labelCarbGrams.AutoSize = true;
            this.labelCarbGrams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCarbGrams.Location = new System.Drawing.Point(13, 100);
            this.labelCarbGrams.Name = "labelCarbGrams";
            this.labelCarbGrams.Size = new System.Drawing.Size(187, 20);
            this.labelCarbGrams.TabIndex = 1;
            this.labelCarbGrams.Text = "Grams of Carbohydrates:";
            // 
            // textBoxFatGrams
            // 
            this.textBoxFatGrams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFatGrams.Location = new System.Drawing.Point(206, 49);
            this.textBoxFatGrams.Name = "textBoxFatGrams";
            this.textBoxFatGrams.Size = new System.Drawing.Size(100, 26);
            this.textBoxFatGrams.TabIndex = 1;
            // 
            // textBoxCarbGrams
            // 
            this.textBoxCarbGrams.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCarbGrams.Location = new System.Drawing.Point(206, 100);
            this.textBoxCarbGrams.Name = "textBoxCarbGrams";
            this.textBoxCarbGrams.Size = new System.Drawing.Size(100, 26);
            this.textBoxCarbGrams.TabIndex = 2;
            // 
            // buttonCalculateCalories
            // 
            this.buttonCalculateCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculateCalories.Location = new System.Drawing.Point(114, 208);
            this.buttonCalculateCalories.Name = "buttonCalculateCalories";
            this.buttonCalculateCalories.Size = new System.Drawing.Size(165, 31);
            this.buttonCalculateCalories.TabIndex = 3;
            this.buttonCalculateCalories.Text = "Calculate Calories";
            this.buttonCalculateCalories.UseVisualStyleBackColor = true;
            this.buttonCalculateCalories.Click += new System.EventHandler(this.buttonCalculateCalories_Click);
            // 
            // labelTotalCalories
            // 
            this.labelTotalCalories.AutoSize = true;
            this.labelTotalCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotalCalories.Location = new System.Drawing.Point(85, 268);
            this.labelTotalCalories.Name = "labelTotalCalories";
            this.labelTotalCalories.Size = new System.Drawing.Size(109, 20);
            this.labelTotalCalories.TabIndex = 2;
            this.labelTotalCalories.Text = "Total Calories:";
            // 
            // textBoxTotalCalories
            // 
            this.textBoxTotalCalories.Enabled = false;
            this.textBoxTotalCalories.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxTotalCalories.Location = new System.Drawing.Point(208, 265);
            this.textBoxTotalCalories.Name = "textBoxTotalCalories";
            this.textBoxTotalCalories.Size = new System.Drawing.Size(116, 26);
            this.textBoxTotalCalories.TabIndex = 3;
            this.textBoxTotalCalories.TabStop = false;
            this.textBoxTotalCalories.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 322);
            this.Controls.Add(this.textBoxTotalCalories);
            this.Controls.Add(this.labelTotalCalories);
            this.Controls.Add(this.buttonCalculateCalories);
            this.Controls.Add(this.groupBoxGramsPerDay);
            this.Name = "Form1";
            this.Text = "Calorie Consumption Calculator";
            this.groupBoxGramsPerDay.ResumeLayout(false);
            this.groupBoxGramsPerDay.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxGramsPerDay;
        private System.Windows.Forms.TextBox textBoxCarbGrams;
        private System.Windows.Forms.TextBox textBoxFatGrams;
        private System.Windows.Forms.Label labelCarbGrams;
        private System.Windows.Forms.Label labelFatGrams;
        private System.Windows.Forms.Button buttonCalculateCalories;
        private System.Windows.Forms.Label labelTotalCalories;
        private System.Windows.Forms.TextBox textBoxTotalCalories;
    }
}


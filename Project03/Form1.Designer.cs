/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 3
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a button, labels, textbox, and the 
 * built-in openFileDialog box. Allow application to select a text
 * file, read each line and convert them to lower case, parse out
 * each word capturing lowest/highest alphabetical word, longest
 * word, and word with most vowels. Display these words in the
 * textbox and then save the statistics to a text file.
 * 
 */

namespace Project03
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonSelectFile = new System.Windows.Forms.Button();
            this.labelFileStats = new System.Windows.Forms.Label();
            this.textBoxFileStatistics = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonSelectFile
            // 
            this.buttonSelectFile.Location = new System.Drawing.Point(28, 25);
            this.buttonSelectFile.Name = "buttonSelectFile";
            this.buttonSelectFile.Size = new System.Drawing.Size(75, 23);
            this.buttonSelectFile.TabIndex = 0;
            this.buttonSelectFile.Text = "Select File";
            this.buttonSelectFile.UseVisualStyleBackColor = true;
            this.buttonSelectFile.Click += new System.EventHandler(this.buttonSelectFile_Click);
            // 
            // labelFileStats
            // 
            this.labelFileStats.AutoSize = true;
            this.labelFileStats.Location = new System.Drawing.Point(28, 75);
            this.labelFileStats.Name = "labelFileStats";
            this.labelFileStats.Size = new System.Drawing.Size(71, 13);
            this.labelFileStats.TabIndex = 1;
            this.labelFileStats.Text = "File Statistics:";
            // 
            // textBoxFileStatistics
            // 
            this.textBoxFileStatistics.AcceptsReturn = true;
            this.textBoxFileStatistics.Font = new System.Drawing.Font("Courier New", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxFileStatistics.Location = new System.Drawing.Point(31, 91);
            this.textBoxFileStatistics.Multiline = true;
            this.textBoxFileStatistics.Name = "textBoxFileStatistics";
            this.textBoxFileStatistics.Size = new System.Drawing.Size(758, 144);
            this.textBoxFileStatistics.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(825, 268);
            this.Controls.Add(this.textBoxFileStatistics);
            this.Controls.Add(this.labelFileStats);
            this.Controls.Add(this.buttonSelectFile);
            this.Name = "Form1";
            this.Text = "Read Write Text";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonSelectFile;
        private System.Windows.Forms.Label labelFileStats;
        private System.Windows.Forms.TextBox textBoxFileStatistics;
    }
}


/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 3
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a button, labels, textbox, and the 
 * built-in openFileDialog box. Allow application to select a text
 * file, read each line and convert them to lower case, parse out
 * each word capturing lowest/highest alphabetical word, longest
 * word, and word with most vowels. Display these words in the
 * textbox and then save the statistics to a text file.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project03
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}

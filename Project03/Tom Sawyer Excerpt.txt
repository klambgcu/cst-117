"Hi- yi ! You're up a stump, ain't you!"
No answer. Tom surveyed his last touch with the eye of an artist, then he gave
his brush another gentle sweep and surveyed the result, as before. Ben ranged
up alongside of him. Tom's mouth watered for the apple, but he stuck to his
work. Ben said:
"Hello, old chap, you got to work, hey?"
Tom wheeled suddenly and said:
"Why, it's you, Ben! I warn't noticing."
"Say -- I'm going in a-swimming, I am. Don't you wish you could? But of course
you'd druther work -- wouldn't you? Course you would!"
Tom contemplated the boy a bit, and said:
"What do you call work?"
"Why, ain't that work?"

Tom resumed his whitewashing, and answered carelessly:
"Well, maybe it is, and maybe it ain't. All I know, is, it suits Tom Sawyer."
"Oh come, now, you don't mean to let on that you like it?"
The brush continued to move.
"Like it? Well, I don't see why I oughtn't to like it. Does a boy get a chance
to whitewash a fence every day?"
That put the thing in a new light. Ben stopped nibbling his apple. 
Tom swept his brush daintily back and forth -- stepped back to note the 
effect -- added a touch here and there -- criticised the effect again -- Ben 
watching every move and getting more and more interested, more and more absorbed. 
Presently he said:
"Say, Tom, let me whitewash a little."
Tom considered, was about to consent; but he altered his mind:
"No -- no -- I reckon it wouldn't hardly do, Ben. You see, Aunt Polly's awful 
particular about this fence -- right here on the street, you know -- but if it 
was the back fence I wouldn't mind and she wouldn't. Yes, she's awful particular 
about this fence; it's got to be done very careful; I reckon there ain't one boy 
in a thousand, maybe two thousand, that can do it the way it's got to be done."
"No -- is that so? Oh come, now -- lemme

just try. Only just a little -- I'd let you , if you was me, Tom." 
"Ben, I'd like to, honest injun; but Aunt Polly -- well, Jim wanted to do it, but 
she wouldn't let him; Sid wanted to do it, and she wouldn't let Sid. Now don't you
see how I'm fixed? If you was to tackle this fence and anything was to happen to 
it -- "
"Oh, shucks, I'll be just as careful. Now lemme try. Say -- I'll give you the core 
of my apple."
"Well, here -- No, Ben, now don't. I'm afeard -- "
"I'll give you all of it!"
Tom gave up the brush with reluctance in his face, but alacrity in his heart. 
And while the late steamer Big Missouri worked and sweated in the sun, the retired 
artist sat on a barrel in the shade close by, dangled his legs, munched his apple, 
and planned the slaughter of more innocents. There was no lack of material; boys 
happened along every little while; they came to jeer, but remained to whitewash. 
By the time Ben was fagged out, Tom had traded the next chance to Billy Fisher for 
a kite, in good repair; and when he played out, Johnny Miller bought in for a dead 
rat and a string to swing it with -- and so on, and so on, hour after hour. And when 
the middle of the afternoon came, from being a poor poverty-stricken boy in the 
morning, Tom was literally rolling in wealth. He had besides the things before 
mentioned, twelve marbles,

part of a jews-harp, a piece of blue bottle-glass to look through, a spool cannon, 
a key that wouldn't unlock anything, a fragment of chalk, a glass stopper of a 
decanter, a tin soldier, a couple of tadpoles, six fire-crackers, a kitten with 
only one eye, a brass door-knob, a dog-collar -- but no dog -- the handle of a 
knife, four pieces of orange-peel, and a dilapidated old window sash. 
He had had a nice, good, idle time all the while -- plenty of company -- and the 
fence had three coats of whitewash on it! If he hadn't run out of whitewash he 
would have bankrupted every boy in the village.
Tom said to himself that it was not such a hollow world, after all. He had discovered 
a great law of human action, without knowing it -- namely, that in order to make a 
man or a boy covet a thing, it is only necessary to make the thing difficult to 
attain. If he had been a great and wise philosopher, like the writer of this book,
he would now have comprehended that Work consists of whatever a body is obliged to 
do, and that Play consists of whatever a body is not obliged to do. And this would 
help him to understand why constructing artificial flowers or performing on a 
tread-mill is work, while rolling ten-pins or climbing Mont Blanc is only 
amusement. There are wealthy gentlemen in England who drive four-horse 
passenger-coaches twenty or thirty miles on a daily line, in the summer, because 
the privilege costs them considerable money; but if they were offered wages for 
the service, that would turn it into work and then they would resign. 
The boy mused awhile over the substantial change which had taken place in his 
worldly circumstances, and then wended toward headquarters to report.

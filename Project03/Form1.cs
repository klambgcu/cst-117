/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 3
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a button, labels, textbox, and the 
 * built-in openFileDialog box. Allow application to select a text
 * file, read each line and convert them to lower case, parse out
 * each word capturing lowest/highest alphabetical word, longest
 * word, and word with most vowels. Display these words in the
 * textbox and then save the statistics to a text file.
 * 
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace Project03
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonSelectFile_Click(object sender, EventArgs e)
        {
            // Define variables to open and parse the selected file
            var filePath = string.Empty;
            string fileName = string.Empty;
            string alphaFirstWord = string.Empty;
            string alphaLastWord = string.Empty;
            string mostVowelsWord = string.Empty;
            string longestWord = string.Empty;
            int vowelCount = 0;

            // Open dialog box to select file
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.InitialDirectory = "c:\\";
                openFileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                openFileDialog.FilterIndex = 1;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path and name of specified file
                    filePath = openFileDialog.FileName;
                    fileName = openFileDialog.SafeFileName;

                    // Open file and loop through it - read each line
                    foreach (string line in File.ReadLines(filePath))
                    {
                        // Convert line to lowercase and remove blank spaces
                        string lowercaseLine = line.ToLower().Trim();
                        // Ignore empty lines
                        if (lowercaseLine == string.Empty)
                            continue;

                        // Split line into words
                        // Use Regular Expression to split on all non-word characters
                        // @   Special verbatim string syntax.
                        // \W+ One or more non - word characters together.
                        string[] words = Regex.Split(lowercaseLine, @"\W+");

                        // Loop through list of words
                        foreach (string aWord in words)
                        {
                            // Clean up the word
                            string word = aWord.Trim();
                            // Ignore empty words
                            if (word == string.Empty)
                                continue;

                            // Capture Alphabetical First Word
                            if (alphaFirstWord == string.Empty || word.CompareTo(alphaFirstWord) < 0)
                                alphaFirstWord = word;

                            // Capture Alphabetical Last Word
                            if (alphaLastWord == string.Empty || word.CompareTo(alphaLastWord) > 0)
                                alphaLastWord = word;

                            // Capture Longest Word
                            if (word.Length > longestWord.Length)
                                longestWord = word;

                            // Capture word containing most vowels
                            // Check each character in word for vowel - increment vowel total
                            int wordVowelTotal = 0;
                            foreach (char ch in word)
                                if ("aeiou".Contains(ch))
                                    wordVowelTotal++;

                            // Check vowel totals and store word if greater
                            if (wordVowelTotal > vowelCount)
                            {
                                vowelCount = wordVowelTotal;
                                mostVowelsWord = word;
                            }
                        }
                    }
                    // Clear Textbox and Add Filename
                    textBoxFileStatistics.Clear();
                    textBoxFileStatistics.AppendText("Filename                : " + fileName + Environment.NewLine);

                    // Insert statistics into textbox
                    textBoxFileStatistics.AppendText("Alphabetical First Word : " + alphaFirstWord + Environment.NewLine);
                    textBoxFileStatistics.AppendText("Alphabetical Last Word  : " + alphaLastWord + Environment.NewLine);
                    textBoxFileStatistics.AppendText("Longest Word            : " + longestWord + Environment.NewLine);
                    textBoxFileStatistics.AppendText("Most Vowels Word        : " + mostVowelsWord + Environment.NewLine);

                    // Create a file and store the above statistics into it.
                    try
                    {
                        // Declare a StreamWriter variable
                        StreamWriter outputFile;

                        // Create a file and get a StreamWriter object
                        outputFile = File.CreateText("TextStatistics.txt");

                        // Write the information to the file
                        string[] textBoxLines = textBoxFileStatistics.Lines;
                        foreach (string line in textBoxLines)
                        {
                            outputFile.WriteLine(line);
                        }

                        // Close the file
                        outputFile.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + " " + ex.Source);
                    }
                }
            }
        }
    }
}

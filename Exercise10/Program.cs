﻿/*
 * Name      : Kelly Lamb
 * Date      : 2020-02-19
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 6-10
 * ---------------------------------------------------------------
 * Description:
 * Create a console application that reads a text file and counts
 * the number of words ending in t or e (not case sensitive).
 * We’ll say that a t or e is at the end of a word if there is not
 * an alphabetic letter immediately following it.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Exercise10
{
    class Program
    {
        static void Main(string[] args)
        {
            int countTE = 0;

            // Define variables to open and parse the selected file
            string fileName = "Ex10InputFile.txt";
            var filePath = @"C:\Users\Kelly Lamb\source\repos\Exercise10\bin\Debug\" + fileName;

            // Open file and loop through it - read each line
            foreach (string line in File.ReadLines(filePath))
            {
                // Convert line to lowercase to ease comparison
                string lowercaseLine = line.ToLower();

                // Ignore empty lines
                if (lowercaseLine == string.Empty)
                    continue;

                for (int pos = 0; pos < line.Length-1; pos++)
                {
                    if ((line[pos] == 't' || line[pos] == 'e') && !char.IsLetter(line[pos + 1]))
                        countTE++;
                }

             }

            // Display a message to the console information number of words found ending in t or e.
            Console.WriteLine("There are " + countTE.ToString() + " words that end in t or e.");
        }
    }
}

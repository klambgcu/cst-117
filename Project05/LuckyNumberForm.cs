﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-23
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 7-5
 * ---------------------------------------------------------------
 * Description:
 * Create application with multiple forms. Mainform accepts
 * user input, calculates a lucky number and passes that 
 * to a second form (LuckyNumberForm) to display.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project05
{
    public partial class LuckyNumberForm : Form
    {
        public LuckyNumberForm(int luckyNumber)
        {
            InitializeComponent();
            labelLuckyNumber.Text = luckyNumber.ToString();
        }
    }
}

﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-23
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 7-5
 * ---------------------------------------------------------------
 * Description:
 * Create application with multiple forms. Mainform accepts
 * user input, calculates a lucky number and passes that 
 * to a second form (LuckyNumberForm) to display.
 */

namespace Project05
{
    partial class Mainform
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelBrithYear = new System.Windows.Forms.Label();
            this.labelBirthMonth = new System.Windows.Forms.Label();
            this.labelBirthDay = new System.Windows.Forms.Label();
            this.labelFavoriteColor = new System.Windows.Forms.Label();
            this.labelZodiacSign = new System.Windows.Forms.Label();
            this.comboBoxYear = new System.Windows.Forms.ComboBox();
            this.comboBoxMonth = new System.Windows.Forms.ComboBox();
            this.comboBoxDay = new System.Windows.Forms.ComboBox();
            this.comboBoxColor = new System.Windows.Forms.ComboBox();
            this.labelZodiacAnswer = new System.Windows.Forms.Label();
            this.buttonLuckyNumber = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelBrithYear
            // 
            this.labelBrithYear.AutoSize = true;
            this.labelBrithYear.Location = new System.Drawing.Point(50, 41);
            this.labelBrithYear.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelBrithYear.Name = "labelBrithYear";
            this.labelBrithYear.Size = new System.Drawing.Size(179, 24);
            this.labelBrithYear.TabIndex = 0;
            this.labelBrithYear.Text = "Enter your birth year";
            // 
            // labelBirthMonth
            // 
            this.labelBirthMonth.AutoSize = true;
            this.labelBirthMonth.Location = new System.Drawing.Point(33, 104);
            this.labelBirthMonth.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelBirthMonth.Name = "labelBirthMonth";
            this.labelBirthMonth.Size = new System.Drawing.Size(196, 24);
            this.labelBirthMonth.TabIndex = 1;
            this.labelBirthMonth.Text = "Enter your birth month";
            // 
            // labelBirthDay
            // 
            this.labelBirthDay.AutoSize = true;
            this.labelBirthDay.Location = new System.Drawing.Point(56, 168);
            this.labelBirthDay.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelBirthDay.Name = "labelBirthDay";
            this.labelBirthDay.Size = new System.Drawing.Size(173, 24);
            this.labelBirthDay.TabIndex = 2;
            this.labelBirthDay.Text = "Enter your birth day";
            // 
            // labelFavoriteColor
            // 
            this.labelFavoriteColor.AutoSize = true;
            this.labelFavoriteColor.Location = new System.Drawing.Point(21, 233);
            this.labelFavoriteColor.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelFavoriteColor.Name = "labelFavoriteColor";
            this.labelFavoriteColor.Size = new System.Drawing.Size(208, 24);
            this.labelFavoriteColor.TabIndex = 3;
            this.labelFavoriteColor.Text = "Enter your favorite color";
            // 
            // labelZodiacSign
            // 
            this.labelZodiacSign.AutoSize = true;
            this.labelZodiacSign.Location = new System.Drawing.Point(61, 294);
            this.labelZodiacSign.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.labelZodiacSign.Name = "labelZodiacSign";
            this.labelZodiacSign.Size = new System.Drawing.Size(168, 24);
            this.labelZodiacSign.TabIndex = 4;
            this.labelZodiacSign.Text = "Your zodiac sign is";
            // 
            // comboBoxYear
            // 
            this.comboBoxYear.FormattingEnabled = true;
            this.comboBoxYear.Location = new System.Drawing.Point(250, 38);
            this.comboBoxYear.Margin = new System.Windows.Forms.Padding(6);
            this.comboBoxYear.Name = "comboBoxYear";
            this.comboBoxYear.Size = new System.Drawing.Size(219, 32);
            this.comboBoxYear.TabIndex = 5;
            this.comboBoxYear.SelectedIndexChanged += new System.EventHandler(this.comboBoxYear_SelectedIndexChanged);
            // 
            // comboBoxMonth
            // 
            this.comboBoxMonth.FormattingEnabled = true;
            this.comboBoxMonth.Location = new System.Drawing.Point(250, 101);
            this.comboBoxMonth.Margin = new System.Windows.Forms.Padding(6);
            this.comboBoxMonth.Name = "comboBoxMonth";
            this.comboBoxMonth.Size = new System.Drawing.Size(219, 32);
            this.comboBoxMonth.TabIndex = 6;
            this.comboBoxMonth.SelectedIndexChanged += new System.EventHandler(this.comboBoxMonth_SelectedIndexChanged);
            // 
            // comboBoxDay
            // 
            this.comboBoxDay.FormattingEnabled = true;
            this.comboBoxDay.Location = new System.Drawing.Point(250, 165);
            this.comboBoxDay.Margin = new System.Windows.Forms.Padding(6);
            this.comboBoxDay.Name = "comboBoxDay";
            this.comboBoxDay.Size = new System.Drawing.Size(219, 32);
            this.comboBoxDay.TabIndex = 7;
            this.comboBoxDay.SelectedIndexChanged += new System.EventHandler(this.comboBoxDay_SelectedIndexChanged);
            // 
            // comboBoxColor
            // 
            this.comboBoxColor.FormattingEnabled = true;
            this.comboBoxColor.Location = new System.Drawing.Point(250, 230);
            this.comboBoxColor.Margin = new System.Windows.Forms.Padding(6);
            this.comboBoxColor.Name = "comboBoxColor";
            this.comboBoxColor.Size = new System.Drawing.Size(219, 32);
            this.comboBoxColor.TabIndex = 8;
            this.comboBoxColor.SelectedIndexChanged += new System.EventHandler(this.comboBoxColor_SelectedIndexChanged);
            // 
            // labelZodiacAnswer
            // 
            this.labelZodiacAnswer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelZodiacAnswer.Enabled = false;
            this.labelZodiacAnswer.Location = new System.Drawing.Point(250, 291);
            this.labelZodiacAnswer.Name = "labelZodiacAnswer";
            this.labelZodiacAnswer.Size = new System.Drawing.Size(219, 31);
            this.labelZodiacAnswer.TabIndex = 9;
            this.labelZodiacAnswer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonLuckyNumber
            // 
            this.buttonLuckyNumber.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonLuckyNumber.Location = new System.Drawing.Point(25, 360);
            this.buttonLuckyNumber.Name = "buttonLuckyNumber";
            this.buttonLuckyNumber.Size = new System.Drawing.Size(444, 78);
            this.buttonLuckyNumber.TabIndex = 10;
            this.buttonLuckyNumber.Text = "Get Your Lucky Number";
            this.buttonLuckyNumber.UseVisualStyleBackColor = false;
            this.buttonLuckyNumber.Click += new System.EventHandler(this.buttonLuckyNumber_Click);
            // 
            // Mainform
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 471);
            this.Controls.Add(this.buttonLuckyNumber);
            this.Controls.Add(this.labelZodiacAnswer);
            this.Controls.Add(this.comboBoxColor);
            this.Controls.Add(this.comboBoxDay);
            this.Controls.Add(this.comboBoxMonth);
            this.Controls.Add(this.comboBoxYear);
            this.Controls.Add(this.labelZodiacSign);
            this.Controls.Add(this.labelFavoriteColor);
            this.Controls.Add(this.labelBirthDay);
            this.Controls.Add(this.labelBirthMonth);
            this.Controls.Add(this.labelBrithYear);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "Mainform";
            this.Text = "Lucky Numbers!";
            this.Load += new System.EventHandler(this.Mainform_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelBrithYear;
        private System.Windows.Forms.Label labelBirthMonth;
        private System.Windows.Forms.Label labelBirthDay;
        private System.Windows.Forms.Label labelFavoriteColor;
        private System.Windows.Forms.Label labelZodiacSign;
        private System.Windows.Forms.ComboBox comboBoxYear;
        private System.Windows.Forms.ComboBox comboBoxMonth;
        private System.Windows.Forms.ComboBox comboBoxDay;
        private System.Windows.Forms.ComboBox comboBoxColor;
        private System.Windows.Forms.Label labelZodiacAnswer;
        private System.Windows.Forms.Button buttonLuckyNumber;
    }
}


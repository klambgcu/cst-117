﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-23
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 7-5
 * ---------------------------------------------------------------
 * Description:
 * Create application with multiple forms. Mainform accepts
 * user input, calculates a lucky number and passes that 
 * to a second form (LuckyNumberForm) to display.
 */

namespace Project05
{
    partial class LuckyNumberForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelLuckyNumber = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelLuckyNumber
            // 
            this.labelLuckyNumber.BackColor = System.Drawing.Color.Transparent;
            this.labelLuckyNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelLuckyNumber.Location = new System.Drawing.Point(135, 267);
            this.labelLuckyNumber.Name = "labelLuckyNumber";
            this.labelLuckyNumber.Size = new System.Drawing.Size(147, 84);
            this.labelLuckyNumber.TabIndex = 0;
            this.labelLuckyNumber.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LuckyNumber
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Project05.Properties.Resources.LuckyNumber;
            this.ClientSize = new System.Drawing.Size(404, 381);
            this.Controls.Add(this.labelLuckyNumber);
            this.Name = "LuckyNumber";
            this.Text = "Your Results!";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelLuckyNumber;
    }
}
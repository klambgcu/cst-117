﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-23
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 7-5
 * ---------------------------------------------------------------
 * Description:
 * Create application with multiple forms. Mainform accepts
 * user input, calculates a lucky number and passes that 
 * to a second form (LuckyNumberForm) to display.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project05
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Mainform());
        }
    }
}

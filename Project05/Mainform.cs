﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-23
 * Professor : Dr. Aiman Darwiche
 * Assignment: Project 7-5
 * ---------------------------------------------------------------
 * Description:
 * Create application with multiple forms. Mainform accepts
 * user input, calculates a lucky number and passes that 
 * to a second form (LuckyNumberForm) to display.
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project05
{
    public partial class Mainform : Form
    {
        // Declare and populate variables
        public bool leapYear = false;
        public int currentYear = DateTime.Now.Year;
        public int luckyNumber = 1;
        public int selectedColor = -1;
        public int selectedDay = -1;
        public int selectedMonth = -1;
        public int selectedYear = -1;
        public int[] monthDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
        public int[] zodiacDate = { 19, 18, 20, 19, 20, 20, 22, 22, 22, 22, 21, 21 };
        public string[] colors = { "Blue", "Green", "Yellow", "Orange", "Red", "Purple", "Brown", "Black", "Grey", "White" };
        public string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        public string[] zodiacName = { "Capricorn", "Aquarius", "Pisces", "Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn" };

        public Mainform()
        {
            InitializeComponent();
        }

        private void Mainform_Load(object sender, EventArgs e)
        {
            // Populate Year Combobox - 120 for longevity :)
            for (int loop = currentYear; loop >= (currentYear - 120); loop--)
            {
                comboBoxYear.Items.Add(loop);
            }

            // Populate Month Combobox
            foreach (string month in months)
            {
                comboBoxMonth.Items.Add(month);
            }

            // Populate Color Combobox
            foreach (string color in colors)
            {
                comboBoxColor.Items.Add(color);
            }

            // Clear Day Combobox
            comboBoxDay.Items.Clear();

            // Disable Lucky Button
            buttonLuckyNumber.Enabled = false;
        }

        private void comboBoxYear_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set variables, fill day field box, and check if button should be enabled
            selectedYear = int.Parse(comboBoxYear.SelectedItem.ToString());
            leapYear = DateTime.IsLeapYear(selectedYear);
            populateComboBoxDay();
            checkLuckyButton();
        }

        private void comboBoxMonth_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set variables, fill day field box, and check if button should be enabled
            selectedMonth = comboBoxMonth.SelectedIndex;
            populateComboBoxDay();
            checkLuckyButton();
        }

        private void comboBoxDay_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set variables, set zodiac, and check if button should be enabled
            selectedDay = int.Parse(comboBoxDay.SelectedItem.ToString());
            populateZodiacAnswer();
            checkLuckyButton();
        }

        private void comboBoxColor_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Set variables and check if button should be enabled
            selectedColor = comboBoxColor.SelectedIndex;
            checkLuckyButton();
        }

        // Programmatically populate the Day box based depending on leap year/month
        private void populateComboBoxDay()
        {
            // Clear out dependent fields until a day is selected
            comboBoxDay.Items.Clear();
            comboBoxDay.Text = "";
            selectedDay = -1;
            labelZodiacAnswer.Text = "";

            // Year and Month must be chosen before populating Day
            if (selectedMonth > -1 && selectedYear > -1)
            {

                // Populate Combobox Day
                int numDays = monthDays[selectedMonth];
                if (leapYear && selectedMonth == 1)
                {
                    numDays++;
                }

                for (int i = 1; i <= numDays; i++)
                {
                    comboBoxDay.Items.Add(i);
                }
            }
        }

        // Determine Zodiac and populate the label
        private void populateZodiacAnswer()
        {
            if (selectedDay <= zodiacDate[selectedMonth])
                labelZodiacAnswer.Text = zodiacName[selectedMonth];
            else
                labelZodiacAnswer.Text = zodiacName[selectedMonth + 1];
            checkLuckyButton();
        }

        // Button is disabled until all fields have been entered
        private void checkLuckyButton()
        {
            if (selectedYear > -1 && selectedMonth > -1 && selectedDay > -1 && selectedColor > -1)
            {
                buttonLuckyNumber.Enabled = true;
            }
            else
            {
                buttonLuckyNumber.Enabled = false;
                luckyNumber = 1;
            }
        }

        private void buttonLuckyNumber_Click(object sender, EventArgs e)
        {
            // Calculate new lucky number and display it in another 'fancy' form
            luckyNumber = (selectedYear + selectedMonth + selectedDay + selectedColor) % 20 + 1;

            LuckyNumberForm form = new LuckyNumberForm(luckyNumber);
            form.ShowDialog();
        }
    }
}



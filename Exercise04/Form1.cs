﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Exercise 2-4
 * ---------------------------------------------------------------
 * Description:
 * Create a form that converts a unit of measure to another unit.
 * Convert number of seconds into whole minutes, hours, days
 * if number of seconds is large enough.
 * Utilize TryParse to capture invalid data and display a message
 * 
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btn_convert_Click(object sender, EventArgs e)
        {
            int seconds;

            // Clear any previous conversions
            labelNumberMinutes.Text = "";
            labelNumberHours.Text = "";
            labelNumberDays.Text = "";

            if (int.TryParse(textBoxSeconds.Text, out seconds))
            {
                // Post number of whole minutes if enough seconds
                if (seconds >= 60)
                    labelNumberMinutes.Text = (seconds / 60).ToString();

                // Post number of whole hours if enough seconds
                if (seconds >= 3600)
                    labelNumberHours.Text = (seconds / 3600).ToString();

                // Post number of whole days if enough seconds
                if (seconds >= 86400)
                    labelNumberDays.Text = (seconds / 86400).ToString();
            }
            else
            {
                // Invalid entry for number of seconds - parsing failed - inform user
                MessageBox.Show("Invalid entry for number of seconds", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Error);

                // Reset seconds entry for retry
                textBoxSeconds.Text = "";
            }
        }
    }
}

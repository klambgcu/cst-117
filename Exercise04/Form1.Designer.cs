﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Exercise 2-4
 * ---------------------------------------------------------------
 * Description:
 * Create a form that converts a unit of measure to another unit.
 * Convert number of seconds into whole minutes, hours, days
 * if number of seconds is large enough.
 * Utilize TryParse to capture invalid data and display a message
 * 
 */

namespace Exercise04
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelSeconds = new System.Windows.Forms.Label();
            this.textBoxSeconds = new System.Windows.Forms.TextBox();
            this.btn_convert = new System.Windows.Forms.Button();
            this.labelMinutes = new System.Windows.Forms.Label();
            this.labelHours = new System.Windows.Forms.Label();
            this.labelDays = new System.Windows.Forms.Label();
            this.labelNumberMinutes = new System.Windows.Forms.Label();
            this.labelNumberHours = new System.Windows.Forms.Label();
            this.labelNumberDays = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelSeconds
            // 
            this.labelSeconds.AutoSize = true;
            this.labelSeconds.Location = new System.Drawing.Point(80, 25);
            this.labelSeconds.Name = "labelSeconds";
            this.labelSeconds.Size = new System.Drawing.Size(114, 13);
            this.labelSeconds.TabIndex = 0;
            this.labelSeconds.Text = "Enter Time in Seconds";
            // 
            // textBoxSeconds
            // 
            this.textBoxSeconds.Location = new System.Drawing.Point(206, 22);
            this.textBoxSeconds.MaxLength = 20;
            this.textBoxSeconds.Name = "textBoxSeconds";
            this.textBoxSeconds.Size = new System.Drawing.Size(100, 20);
            this.textBoxSeconds.TabIndex = 1;
            // 
            // btn_convert
            // 
            this.btn_convert.Location = new System.Drawing.Point(206, 57);
            this.btn_convert.Name = "btn_convert";
            this.btn_convert.Size = new System.Drawing.Size(100, 23);
            this.btn_convert.TabIndex = 2;
            this.btn_convert.Text = "Convert";
            this.btn_convert.UseVisualStyleBackColor = true;
            this.btn_convert.Click += new System.EventHandler(this.btn_convert_Click);
            // 
            // labelMinutes
            // 
            this.labelMinutes.AutoSize = true;
            this.labelMinutes.Location = new System.Drawing.Point(64, 95);
            this.labelMinutes.Name = "labelMinutes";
            this.labelMinutes.Size = new System.Drawing.Size(130, 13);
            this.labelMinutes.TabIndex = 3;
            this.labelMinutes.Text = "Number of Whole Minutes";
            // 
            // labelHours
            // 
            this.labelHours.AutoSize = true;
            this.labelHours.Location = new System.Drawing.Point(73, 123);
            this.labelHours.Name = "labelHours";
            this.labelHours.Size = new System.Drawing.Size(121, 13);
            this.labelHours.TabIndex = 4;
            this.labelHours.Text = "Number of Whole Hours";
            // 
            // labelDays
            // 
            this.labelDays.AutoSize = true;
            this.labelDays.Location = new System.Drawing.Point(77, 151);
            this.labelDays.Name = "labelDays";
            this.labelDays.Size = new System.Drawing.Size(117, 13);
            this.labelDays.TabIndex = 5;
            this.labelDays.Text = "Number of Whole Days";
            // 
            // labelNumberMinutes
            // 
            this.labelNumberMinutes.Location = new System.Drawing.Point(206, 95);
            this.labelNumberMinutes.Name = "labelNumberMinutes";
            this.labelNumberMinutes.Size = new System.Drawing.Size(100, 13);
            this.labelNumberMinutes.TabIndex = 6;
            this.labelNumberMinutes.Text = "            ";
            // 
            // labelNumberHours
            // 
            this.labelNumberHours.Location = new System.Drawing.Point(206, 123);
            this.labelNumberHours.Name = "labelNumberHours";
            this.labelNumberHours.Size = new System.Drawing.Size(100, 13);
            this.labelNumberHours.TabIndex = 7;
            this.labelNumberHours.Text = "            ";
            // 
            // labelNumberDays
            // 
            this.labelNumberDays.Location = new System.Drawing.Point(206, 151);
            this.labelNumberDays.Name = "labelNumberDays";
            this.labelNumberDays.Size = new System.Drawing.Size(100, 13);
            this.labelNumberDays.TabIndex = 8;
            this.labelNumberDays.Text = "      ";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(370, 194);
            this.Controls.Add(this.labelNumberDays);
            this.Controls.Add(this.labelNumberHours);
            this.Controls.Add(this.labelNumberMinutes);
            this.Controls.Add(this.labelDays);
            this.Controls.Add(this.labelHours);
            this.Controls.Add(this.labelMinutes);
            this.Controls.Add(this.btn_convert);
            this.Controls.Add(this.textBoxSeconds);
            this.Controls.Add(this.labelSeconds);
            this.Name = "Form1";
            this.Text = "Time Conversion";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSeconds;
        private System.Windows.Forms.TextBox textBoxSeconds;
        private System.Windows.Forms.Button btn_convert;
        private System.Windows.Forms.Label labelMinutes;
        private System.Windows.Forms.Label labelHours;
        private System.Windows.Forms.Label labelDays;
        private System.Windows.Forms.Label labelNumberMinutes;
        private System.Windows.Forms.Label labelNumberHours;
        private System.Windows.Forms.Label labelNumberDays;
    }
}


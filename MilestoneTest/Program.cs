﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-22
 * Professor : Dr. Aiman Darwiche
 * Assignment: Milestone 6-3
 * Note      : I certify that this is my own work.
 * ---------------------------------------------------------------
 * Description:
 * Create an application that has multiple forms and handles all
 * criteria necessary to maintain a list of inventory items. I
 * have chosen to manage equipment inventory for a softball league.
 * Console application to test drive the Inventory Manager and Item
 * classes - add, remove, search, reorder/restock, read/write file
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilestoneTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create inventory manager
            InventoryManager manager = new InventoryManager("InventoryData.txt");

            // Read Data file
            // manager.read();

            Console.Clear();

            // 1. Add inventory
            for (int i = 1; i <= 5; i++)
            {
                int id = manager.NextID;
                manager.add(
                    new InventoryItem
                    {
                        InventoryId = id,
                        EquipmentName = "Name" + id,
                        EquipmentMake = "Make" + id,
                        EquipmentModel = "Model" + id,
                        Description = "Description" + id,
                        Qty = id * 10,
                        Price = id * 0.99M,
                        Location = "Location" + id,
                        OnLoan = "No"
                    });
            }

            Console.WriteLine("1. Add Inventory:");
            // Display Added Inventory
            manager.displayItems(manager.Items);

            // 2. Remove an item
            manager.remove(
                new InventoryItem
                {
                    // Remove inventory with id = 4
                    InventoryId = 4,
                    EquipmentName = "",
                    EquipmentMake = "",
                    EquipmentModel = "",
                    Description = "",
                    Qty = 0,
                    Price = 0M,
                    Location = "",
                    OnLoan = ""
                });

            // Display List of Inventory after #2 was removed
            Console.WriteLine("2. Remove Inventory Item 4:");
            manager.displayItems(manager.Items);

            // 3. Search Items - multiple criteria
            Console.WriteLine("3. Search Inventory (Multiple Criteria: Name1, Make2, Qty 50)");
            manager.displayItems(
                manager.search(
                    new InventoryItem
                    {
                        InventoryId = 0,
                        EquipmentName = "Name1",
                        EquipmentMake = "Make2",
                        EquipmentModel = "",
                        Description = "",
                        Qty = 50,
                        Price = -10M,
                        Location = "",
                        OnLoan = ""
                    }));

            // 4. Reorder test 20 and below
            Console.WriteLine("4. Find items to reorder (Qty 20 and below)");
            List<InventoryItem> list = manager.reorder(20);
            manager.displayItems(list);

            // 5. Restock items found above, then show results
            // Restock another 100 for each item
            foreach (InventoryItem item in list)
                manager.restock(item, 100);

            Console.WriteLine("5. Restock items results");
            manager.displayItems(manager.Items);

            // 6. Edit Inventory
            manager.edit(new InventoryItem(5, "Kelly", "Eugene", "Lamb", "Programmer", 55, 99999.99M, "California", "Yes"));
            Console.WriteLine("6. Edit item 5 results");
            manager.displayItems(manager.Items);

            // Save data to text file
            manager.write();
        }
    }
}

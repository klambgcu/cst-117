﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date      : 2020-02-22
 * Professor : Dr. Aiman Darwiche
 * Assignment: Milestone 6-3
 * Note      : I certify that this is my own work.
 * ---------------------------------------------------------------
 * Description:
 * Create an application that has multiple forms and handles all
 * criteria necessary to maintain a list of inventory items. I
 * have chosen to manage equipment inventory for a softball league.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MilestoneTest
{
    public class InventoryItem
    {

        // Define  Variables
        private int inventory_id;
        private string equipment_name;
        private string equipment_make;
        private string equipment_model;
        private string description;
        private int qty;
        private decimal price;
        private string location;
        private string on_loan;

        // Define Constructors
        public InventoryItem()
        {
            InventoryId = 0;
            EquipmentName = "";
            EquipmentMake = "";
            EquipmentModel = "";
            Description = "";
            Qty = 0;
            Price = 0.00M;
            Location = "";
            OnLoan = "";
        }

        public InventoryItem(int id, string name, string make, string model, string desc, int qty, decimal price, string loc, string on_loan)
        {
            InventoryId = id;
            EquipmentName = name;
            EquipmentMake = make;
            EquipmentModel = model;
            Description = desc;
            Qty = qty;
            Price = price;
            Location = loc;
            OnLoan = on_loan;
        }

        // Define Property - Accessors & Mutators
        public int InventoryId
        {
            get { return inventory_id; }
            set { inventory_id = value; }
        }

        public string EquipmentName
        {
            get { return equipment_name; }
            set { equipment_name = value; }
        }

        public string EquipmentMake
        {
            get { return equipment_make; }
            set { equipment_make = value; }
        }

        public string EquipmentModel
        {
            get { return equipment_model; }
            set { equipment_model = value; }
        }

        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        public int Qty
        {
            get { return qty; }
            set { qty = value; }
        }

        public decimal Price
        {
            get { return price; }
            set { price = value; }
        }

        public string Location
        {
            get { return location; }
            set { location = value; }
        }

        public string OnLoan
        {
            get { return on_loan; }
            set { on_loan = value; }
        }

        // Define TabDelimitedString
        public string TabDelimitedString()
        {
            return  inventory_id.ToString() + "\t" +
                    equipment_name + "\t" +
                    equipment_make + "\t" +
                    equipment_model + "\t" +
                    description + "\t" +
                    qty.ToString() + "\t" +
                    price.ToString() + "\t" +
                    location + "\t" +
                    on_loan + "\t";
        }
    }
}

/*
 * Name	     : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Date	     : 2020-02-25
 * Professor : Dr. Aiman Darwiche
 * Assignment: Milestone 7-4
 * Note	     : I certify that this is my own work.
 * ---------------------------------------------------------------
 * Description:
 * Create an application that has multiple forms and handles all
 * criteria necessary to maintain a list of inventory items. I
 * have chosen to manage equipment inventory for a softball league.
 * This class is for managing inventory items.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace MilestoneTest
{
	public class InventoryManager
	{

		// Define  Variables
		private List<InventoryItem> items;
		private string fileName = "";
		private int nextID = 1;

		// Define Constructors
		public InventoryManager()
		{
			items = new List<InventoryItem>();
		}

		public InventoryManager(string fileName)
		{
			this.fileName = fileName;
			items = new List<InventoryItem>();
		}

		// Define Property - Accessors & Mutators
		public int Count
		{
			get { return items.Count(); }
		}

		public string FileName
		{
			get { return fileName; }
			set { fileName = value; }
		}

		public List<InventoryItem> Items
		{
			get { return items; }
		}

		public int NextID
		{
			get { return nextID; }
		}

		// Define Member functions
		public bool add(InventoryItem item)
		{
			items.Add(item);
			nextID = item.InventoryId + 1;
			return true;
		}

		// Clear the data array
		public void clear()
		{
			items.Clear();
			nextID = 1;
			return;
		}

		public bool edit(InventoryItem editItem)
		{
			bool found = false;

			// Update item based upon inventory Id
			foreach (InventoryItem item in items)
			{
				if (item.InventoryId == editItem.InventoryId)
				{
					item.EquipmentName = editItem.EquipmentName;
					item.EquipmentModel = editItem.EquipmentModel;
					item.EquipmentMake = editItem.EquipmentMake;
					item.Description = editItem.Description;
					item.Location = editItem.Location;
					item.OnLoan = editItem.OnLoan;
					item.Qty = editItem.Qty;
					item.Price = editItem.Price;
					
					found = true;
					break;
				}
			}
			
			return found;
		}

		// Read the text data file
		public bool read()
		{
			bool fileExists = false;

			if (File.Exists(fileName))
			{
				fileExists = true;

				try
				{
					StreamReader inputFile = File.OpenText(fileName);
					int count = int.Parse(inputFile.ReadLine());

					for (int i = 0; i < count; i++)
					{
						string line = inputFile.ReadLine();
						string[] values = line.Split('\t');

						int id = int.Parse(values[0]);
						int qty = int.Parse(values[5]);
						decimal price = decimal.Parse(values[6]);

						items.Add( new InventoryItem(id, values[1], values[2], values[3], values[4], qty, price, values[7], values[8]) );

						if (id > nextID) nextID = id;
					}
					inputFile.Close();
					nextID++;
				}
				catch (Exception ex)
				{
					Console.WriteLine("Error reading data file. " + ex.Message);
					return false;
				}
			}

			return fileExists;
		}

		// Remove item based upon inventory id
		public bool remove(InventoryItem removeItem)
		{
			bool found = false;

			// Find item based upon inventory Id
			foreach (InventoryItem item in items)
			{
				if (item.InventoryId == removeItem.InventoryId)
				{
					found = items.Remove(item);
					break;
				}
			}
			
			return found;
		}

		// Find a list of items below qty level
		public List<InventoryItem> reorder(int qty)
		{
			// Find items with qty at or below value
			// Create a new list, add found items,
			// and return it.
			List<InventoryItem> results = new List<InventoryItem>();
			foreach (InventoryItem item in items)
			{
				if (item.Qty <= qty)
				{
					results.Add(item);
				}
			}

			return results;
		}

		// update stock with additional quantity for restocking purposes
		public void restock(InventoryItem stock, int amount)
		{
			// Find item in data and update the quantity
			foreach (InventoryItem item in items)
			{
				if (item.InventoryId == stock.InventoryId)
				{
					item.Qty += amount;
					break;
				}
			}
			return;
		}

		// Search multiple fields - check is fields contain stock info
		public List<InventoryItem> search(InventoryItem stock)
		{
			// if inventory id = -1, return entire list
			// if inventory id = 0, return list with fields containing stock field entries
			// Create a new list, add found items and return it.
			List<InventoryItem> results = new List<InventoryItem>();

			if (stock.InventoryId < 0)
			{
				return items;
			}
			else
			{
				foreach (InventoryItem item in items)
				{
					// Check name
					if ((stock.EquipmentName != "" && item.EquipmentName.Contains(stock.EquipmentName)) ||
						 (stock.EquipmentModel != "" && item.EquipmentModel.Contains(stock.EquipmentModel)) ||
						 (stock.EquipmentMake != "" && item.EquipmentMake.Contains(stock.EquipmentMake)) ||
						 (stock.Description != "" && item.Description.Contains(stock.Description)) ||
						 (stock.Location != "" && item.Location.Contains(stock.Location)) ||
						 (stock.OnLoan != "" && item.OnLoan.Contains(stock.OnLoan)) ||
						 (stock.Qty >= 0 && item.Qty == stock.Qty) ||
						 (stock.Price >= 0.00M && item.Price == stock.Price))
					{
						results.Add(item);
					}
				}
			}
			return results;
		}

		// Save Data to the text file
		public bool write()
		{
			if (fileName == "") return false;

			try
			{
				StreamWriter outputFile;
				outputFile = File.CreateText(fileName);
				outputFile.WriteLine(items.Count());
				foreach (InventoryItem item in items)
				{
					outputFile.WriteLine(item.TabDelimitedString());
				}
				outputFile.Close();
			}
			catch (Exception ex)
			{
				// Display an error Message
				Console.WriteLine("Error writing file. " + ex.Message);
				return false;
			}

			return true;
		}

		// Console Test Display Method
		public void displayItems(List<InventoryItem> list)
		{
			Console.WriteLine("ID    Name                 Make                 Model                Description                              Qty   Price      Location             On Loan");
			Console.WriteLine("----- -------------------- -------------------- -------------------- ---------------------------------------- ----- ---------- -------------------- -------");
			foreach (InventoryItem item in list)
			{
				Console.WriteLine("{0,5:D} {1,-20} {2,-20} {3,-20} {4,-40} {5,5:D} {6,10:C} {7,-20} {8,-7}",
									 item.InventoryId,
									 item.EquipmentName,
									 item.EquipmentMake,
									 item.EquipmentModel,
									 item.Description,
									 item.Qty,
									 item.Price,
									 item.Location,
									 item.OnLoan);
			}
			Console.WriteLine();
		}
	}
}

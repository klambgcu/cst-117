﻿using Newtonsoft.Json;
using System.Web.Mvc;
using testJson.Models;
using testJson.Services;

namespace testJson.Controllers
{
    public class HomeController : Controller
    {
        private GameService gameService;
        public HomeController()
        {
            gameService = new GameService();
            gameService.details = new GameDetail();
            gameService.board = new Board();
        }
        public ContentResult Index()
        {
            string json = JsonConvert.SerializeObject(gameService, Formatting.Indented);

            GameService test = (GameService)JsonConvert.DeserializeObject<GameService>(json);

            string json1 = JsonConvert.SerializeObject(test, Formatting.Indented);


            return Content(json1, "application / json");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
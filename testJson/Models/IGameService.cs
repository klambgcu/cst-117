﻿
using System.Drawing;

namespace testJson.Models
{
    // Define states for game loop logic
    public enum GameState
    {
        GamePlaying,
        GameOverWin,
        GameOverLose
    }

    public interface IGameService
    {
        Level getLevel();

        Board getGameBoard();

        GameDetail getGameDetail();

        void ConfigureBoard();

        Point getLastClickedButton();

        GameState GetGameState();

        void NewGame();

        void Hint();

        void handleLeftClick(int row, int col);

        void handleRightClick(int row, int col);

        int getSeconds();

        void GameOverTimeExceeded();
    }
}

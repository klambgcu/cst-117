/*
 * ---------------------------------------------------------------
 * Name      : (Part of Group Team 3)
 * Date      : 2020-11-09
 * Class     : CST-247 Enterprise Computer Programming III
 * Professor : Dinesh Staphit
 * Assignment: Milestone 5
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Stores details for the game progress
 * ---------------------------------------------------------------
 */

using System;

namespace testJson.Models
{
    public class GameDetail
    {
        public GameDetail() { }
        public int Clicks { get; set; } = 0;
        public int Hints { get; set; } = 0;
        public int Seconds { get; set; } = 0;
        public DateTime StartTime { get; set; } = DateTime.Now;
    }
}

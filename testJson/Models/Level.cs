/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Newtonsoft.Json;

namespace testJson.Models
{
    public class Level : ILevel
    {
        public Level() { }

        // Board Size, Bomb Percentage, Bomb Count
        private int[,] gameLevels = { { 10, 10, 10 },
                                      { 15, 18, 40 },
                                      { 20, 25, 100 } };
        public int Id { get; set; } = 0;
        [JsonIgnore]
        public int BoardSize
        {
            get
            {
                return gameLevels[Id, 0];
            }
            set { }
        }
        [JsonIgnore]
        public int BombCount
        {
            get
            {
                return gameLevels[Id, 2];
            }

            set { }
        }
        [JsonIgnore]
        public int BombPercentage
        {
            get
            {
                return gameLevels[Id, 1];
            }
            set { }
        }

        public string Description()
        {
            return Description(Id);
        }

        public string Description(int id)
        {
            string[] levelNames = { "Easy", "Moderate", "Difficult", "All Levels" };
            return levelNames[id];
        }

        public int ConvertLevelToInt(Levels level)
        {
            return (int)level;
        }

        public Levels ConvertIntToLevel(int level)
        {
            return (Levels)level;
        }
    }
}

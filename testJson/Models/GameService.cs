﻿using System;
using System.Drawing;
using testJson.Models;

namespace testJson.Services
{
    public class GameService : IGameService
    {
        private Random random;

        public Level level { get; set; }
        public Board board { get; set; }
        public GameDetail details { get; set; }

        private GameState gameState = GameState.GamePlaying;
        private Point LastClickedButton = new Point(0, 0);

        public GameService() { }
        public GameService(Level level, Board board, GameDetail details)
        {
            // Create random number generator - this method of seeding for variety
            // is the only method on my system that worked appropriately.
            random = new Random((int)Guid.NewGuid().GetHashCode());

            this.level = level;
            this.board = board;
            this.details = details;
            NewGame();
        }

        public Level getLevel()
        {
            return level;
        }

        public Board getGameBoard()
        {
            return board;
        }

        public GameDetail getGameDetail()
        {
            return details;
        }

        public void ConfigureBoard()
        {
            board = new Board(getLevel().BoardSize);
            board.Difficulty = getLevel().BombPercentage;
            setupLiveNeighbors();
            calculateLiveNeighbors();
        }

        private void setLastClickedButton(int row, int col)
        {
            LastClickedButton.X = col;
            LastClickedButton.Y = row;
        }

        public Point getLastClickedButton()
        {
            return LastClickedButton;
        }

        public int getSeconds()
        {
            return details.Seconds;
        }

        public GameState GetGameState()
        {
            return gameState;
        }

        public void GameOverTimeExceeded()
        {
            gameState = GameState.GameOverLose;
            details.Seconds = 999;
        }

        public void NewGame()
        {
            gameState = GameState.GamePlaying;
            ConfigureBoard();
            details.Clicks = 0;
            details.Hints = 0;
            details.Seconds = 0;
            details.StartTime = DateTime.Now;
        }

        public void Hint()
        {
            if (gameState != GameState.GamePlaying) return;

            int size = getLevel().BoardSize;
            int row = 0;
            int col = 0;
            bool found = false;
            do
            {
                col = 0;
                do
                {
                    if ((!board.GetGrid(row, col).Flagged) &&
                         (!board.GetGrid(row, col).Visited) &&
                         (!board.GetGrid(row, col).Live))
                    {
                        found = true;
                        break;
                    }
                    else
                    {
                        col++;
                    }

                } while (col < size && !found);

                if (!found) row++;
            } while (row < size && !found);

            if (found)
            {
                handleLeftClick(row, col);
            }

            details.Hints++;	// increase hints/penalty
            updateSeconds();	// updates the time so it stays accurate
        }

        private void updateSeconds()
        {
            TimeSpan ts = DateTime.Now.Subtract(details.StartTime);
            details.Seconds = (int)Math.Floor(ts.TotalSeconds) + (details.Hints * 15); 	//penalty is 15 seconds for every hint
            if (details.Seconds >= 999)
            {
                details.Seconds = 999;
                gameState = GameState.GameOverLose;
            }
        }

        public void handleLeftClick(int row, int col)
        {
            if (row < 0 ||
                col < 0 ||
                row >= level.BoardSize ||
                col >= level.BoardSize ||
                board.GetGrid(row, col).Flagged)
            {
                return;
            }

            details.Clicks++;

            if (board.GetGrid(row, col).Neighbors > 0)
            {
                setCellVisited(row, col);
                if (board.GetGrid(row, col).Live)
                {
                    gameState = GameState.GameOverLose;
                }
            }
            else
            {
                floodFill(row, col);
            }
            setLastClickedButton(row, col);
            updateSeconds();

            // Check game completed - Board cleared
            if (gameState == GameState.GamePlaying && boardCompleted())
            {
                gameState = GameState.GameOverWin;
            }
        }

        public void handleRightClick(int row, int col)
        {
            if (row < 0 || col < 0 || row >= level.BoardSize || col >= level.BoardSize)
                return;

            toggleFlag(row, col);
            updateSeconds();

            // Check game completed - Board cleared
            if (gameState == GameState.GamePlaying && boardCompleted())
            {
                gameState = GameState.GameOverWin;
            }
        }


        /******************************************************************
         * IN THIS SECTION
         * Moved functionality from Board class to this game service class.
        *******************************************************************/
        // Accessor to set cell visitation and increment visited count
        public void setCellVisited(int row, int col)
        {
            // Cannot visit flagged cells
            if (board.GetGrid(row, col).Flagged) return;

            if (!board.GetGrid(row, col).Visited)
            {
                board.GetGrid(row, col).Visited = true;
                board.VisitedCount++;
            }
        }

        // Recursively check neighbors to expose empty cells (8 Direction)
        // I believe the real game utilized the 8 direction
        // flood fill but the example appears to use 4 direction
        public void floodFill(int row, int col)
        {
            setCellVisited(row, col);

            // Check Orthogonal Directions
            // Check Down
            if (row < level.BoardSize - 1 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row + 1, col).Visited &&
                !board.GetGrid(row + 1, col).Flagged) floodFill(row + 1, col);

            // Check Left
            if (col > 0 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row, col - 1).Visited &&
                !board.GetGrid(row, col - 1).Flagged) floodFill(row, col - 1);

            // Check Up
            if (row > 0 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row - 1, col).Visited &&
                !board.GetGrid(row - 1, col).Flagged) floodFill(row - 1, col);

            // Check Right
            if (col < level.BoardSize - 1 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row, col + 1).Visited &&
                !board.GetGrid(row, col + 1).Flagged) floodFill(row, col + 1);

            // Check Diagonal Directions 
            // Check Down Right
            if (row < level.BoardSize - 1 && col < level.BoardSize - 1 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row + 1, col + 1).Visited &&
                !board.GetGrid(row + 1, col + 1).Flagged) floodFill(row + 1, col + 1);

            // Check Up Left
            if (row > 0 && col > 0 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row - 1, col - 1).Visited &&
                !board.GetGrid(row - 1, col - 1).Flagged) floodFill(row - 1, col - 1);

            // Check Up Right
            if (row > 0 && col < level.BoardSize - 1 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row - 1, col + 1).Visited &&
                !board.GetGrid(row - 1, col + 1).Flagged) floodFill(row - 1, col + 1);

            // Check Down Left
            if (row < level.BoardSize - 1 && col > 0 &&
                board.GetGrid(row, col).Neighbors == 0 &&
                !board.GetGrid(row + 1, col - 1).Visited &&
                !board.GetGrid(row + 1, col - 1).Flagged) floodFill(row + 1, col - 1);
        }

        // Determine (randomly) location for bombs based upon size and difficulty
        public void setupLiveNeighbors()
        {
            board.CellCount = level.BoardSize * level.BoardSize;
            board.BombCount = (int)(board.CellCount * (board.Difficulty / 100.0));
            board.NeutralCount = board.CellCount - board.BombCount;
            for (int i = 0; i < board.BombCount; i++)
            {
                int x = random.Next(board.Size);
                int y = random.Next(board.Size);
                while (board.GetGrid(x, y).Live)
                {
                    x = random.Next(board.Size);
                    y = random.Next(board.Size);
                }
                board.GetGrid(x, y).Live = true;
                board.GetGrid(x, y).Neighbors = 9;
            }
        }

        public void IncrementCellNeighbors(int row, int col)
        {
            if (board.GetGrid(row, col).Live)
            {
                board.GetGrid(row, col).Neighbors = 9;
            }
            else
            {
                board.GetGrid(row, col).Neighbors++;
            }
        }
        // Calculate the number of adjacent cells contain bombs
        public void calculateLiveNeighbors()
        {
            int Size1 = level.BoardSize - 1;
            for (int row = 0; row < level.BoardSize; row++)
            {
                for (int col = 0; col < level.BoardSize; col++)
                {
                    if (board.GetGrid(row, col).Live)
                    {
                        if (row > 0 && col > 0) IncrementCellNeighbors(row - 1, col - 1);
                        if (row > 0) IncrementCellNeighbors(row - 1, col);
                        if (row > 0 && col < Size1) IncrementCellNeighbors(row - 1, col + 1);

                        if (col > 0) IncrementCellNeighbors(row, col - 1);
                        if (col < Size1) IncrementCellNeighbors(row, col + 1);

                        if (row < Size1 && col > 0) IncrementCellNeighbors(row + 1, col - 1);
                        if (row < Size1) IncrementCellNeighbors(row + 1, col);
                        if (row < Size1 && col < Size1) IncrementCellNeighbors(row + 1, col + 1);
                    }
                }
            }
        }

        // Accessor to toggle cell between flagged and not flagged
        public void toggleFlag(int row, int col)
        {
            // Cannot modify flag on visited cells
            if (board.GetGrid(row, col).Visited) return;

            // Cannot flag more cells than number of bombs
            if (board.FlagCount == board.BombCount && !board.GetGrid(row, col).Flagged) return;

            // Toggle cell flag and adjust flag count
            board.GetGrid(row, col).Flagged = !board.GetGrid(row, col).Flagged;
            if (board.GetGrid(row, col).Flagged)
                board.FlagCount++;
            else
                board.FlagCount--;
        }

        // Accessor that determines if all non-bomb cells have been visited
        public bool boardCompleted()
        {
            return (board.FlagCount == board.BombCount && board.VisitedCount == board.NeutralCount);
        }
    }
}
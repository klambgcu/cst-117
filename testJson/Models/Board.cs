/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */


using System;
using System.Collections.Generic;

namespace testJson.Models
{
    public class Board
    {
        public int CellCount { get; set; } = 0;
        public int NeutralCount { get; set; } = 0;
        public int VisitedCount { get; set; } = 0;

        public List<Cell> Grid { get; set; }
        public int Size { get; set; } // Size of square grid
        public int Difficulty { get; set; } // Percentage (0-100) of live bombs
        public int BombCount { get; set; }
        public int FlagCount { get; set; }

        // Default constructor - chain to next constructor
        // Default size = 10
        public Board() : this(10) { }

        // Constructor - main
        public Board(int size)
        {
            Size = size;
            Grid = new List<Cell>();
            for (int row = 0; row < Size; row++)
            {
                for (int col = 0; col < Size; col++)
                {
                    Grid.Add(new Cell(row, col));
                }
            }

            Difficulty = 10; // Default to 10 percent
            CellCount = Size * Size;
            BombCount = (int)(CellCount * (Difficulty / 100.0));
            NeutralCount = CellCount - BombCount;
            FlagCount = 0;
        }

        public Cell GetGrid(int row, int col)
        {
            return Grid[row * Size + col];
        }
    }
}

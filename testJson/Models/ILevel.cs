/*
 * ---------------------------------------------------------------
 * Name      : Kelly Lamb
 * Date      : 2020-09-21
 * Class     : CST-227 Enterprise Computer Programming II
 * Professor : James Shinevar
 * Assignment: Milestone 7
 * Disclaimer: This is my own work
 * ---------------------------------------------------------------
 * Description:
 * 1. Modify existing code with services and Level object
 * 2. OptionsView Form - Select Game options (Level)
 * 3. GameView Form - Contains Game board and Status Panel 
 * 4. HighScoreView Form - Contains Data Grid View and Entry Panel
 * 5. PlayerStats,PlayerStatsManager for Persistent Data
 * Objective:
 * Fully functional game play with high score capture
 * ---------------------------------------------------------------
 */

using Newtonsoft.Json;

namespace testJson.Models
{
    public enum Levels : int
    {
        Easy = 0,
        Moderate = 1,
        Difficult = 2,
        All = 3
    };

    public interface ILevel
    {
        int Id { get; set; }

        [JsonIgnore]
        int BoardSize { get; set; }

        [JsonIgnore]
        int BombCount { get; set; }

        [JsonIgnore]
        int BombPercentage { get; set; }

        string Description();

        string Description(int id);

        int ConvertLevelToInt(Levels level);

        Levels ConvertIntToLevel(int level);
    }
}

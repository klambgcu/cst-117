﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 4-6
 * ---------------------------------------------------------------
 * Description:
 * Create a Dice class - instantiate two Dice objects
 * Create form with a button to roll dice, display dice values
 * and add a list box to view dice roll history. Check results for
 * a pair a 1s = snake eyes and display message with number of
 * rolls. Clear history/roll count on next click after message.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise06
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}

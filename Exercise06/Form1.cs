﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 4-6
 * ---------------------------------------------------------------
 * Description:
 * Create a Dice class - instantiate two Dice objects
 * Create form with a button to roll dice, display dice values
 * and add a list box to view dice roll history. Check results for
 * a pair a 1s = snake eyes and display message with number of
 * rolls. Clear history/roll count on next click after message.
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Exercise06
{
    public partial class Form1 : Form
    {
        // Declare two dice
        private Dice die1 = new Dice(6);
        private Dice die2 = new Dice(6);

        // Counter to track number of times dice are rolled
        private int rollCount = 0;

        // Reset display if playing again
        private bool alreadyWon = false;

        public Form1()
        {
            InitializeComponent();
         }

        private void buttonRollDice_Click(object sender, EventArgs e)
        {
            // Allow user to inspect list after win, but reset on first click after
            if (alreadyWon)
            {
                alreadyWon = false;
                rollCount = 0;
                listBoxRollHistory.Items.Clear();
            }

            // Roll the dice
            int die1Roll = die1.rollDie();
            int die2Roll = die2.rollDie();

            // Display the dice values
            labelDie1.Text = die1Roll.ToString();
            labelDie2.Text = die2Roll.ToString();

            // Increment roll counter
            rollCount++;

            // Add roll results to history display
            listBoxRollHistory.Items.Add("Roll: " + rollCount + ", Die 1 = " + die1Roll + ", Die 2 = " + die2Roll);
            listBoxRollHistory.SetSelected(listBoxRollHistory.Items.Count - 1, true); // select to highlight latest roll

            // Check for snake eyes (both dice are 1) and display message
            if (die1Roll == 1 && die2Roll == 1)
            {
                MessageBox.Show("It took " + rollCount + " rolls to get snake eyes!");
                alreadyWon = true;

                // Rubric says program terminates after message - so call exit not to play again.
                // Remove next line to allow game replay
                System.Windows.Forms.Application.Exit();
            }
        }
    }
}

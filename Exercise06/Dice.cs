﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 4-6
 * ---------------------------------------------------------------
 * Description:
 * Create a Dice class - instantiate two Dice objects
 * Create form with a button to roll dice, display dice values
 * and add a list box to view dice roll history. Check results for
 * a pair a 1s = snake eyes and display message with number of
 * rolls. Clear history/roll count on next click after message.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise06
{
    class Dice
    {
        // Define class variables
        private int _sides;
        private Random random;

        // Parameterized Constructor - clamp sides 4 - 0
        public Dice(int sides)
        {
            // Clamp number of sides to 4 - 20 inclusive
            if (sides < 4)
                _sides = 4;
            else if (sides > 20)
                _sides = 20;
            else
                _sides = sides;

            // Create random number generator - this method of seeding for variety
            // is the only method on my system that worked appropriately.
            random = new Random((int)Guid.NewGuid().GetHashCode());
        }

        // Rolls the dice - returns a random number between 1 and the number of sides inclusive
        // using the Random object Next method.
        public int rollDie()
        {
            return random.Next(1, _sides + 1);
        }
    }
}

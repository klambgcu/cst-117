﻿/*
 * Name      : Kelly Lamb
 * Class     : CST-117 Enterprise Computer Promgramming I
 * Professor : Dr. Aiman Darwiche
 * Assignment: Exercise 4-6
 * ---------------------------------------------------------------
 * Description:
 * Create a Dice class - instantiate two Dice objects
 * Create form with a button to roll dice, display dice values
 * and add a list box to view dice roll history. Check results for
 * a pair a 1s = snake eyes and display message with number of
 * rolls. Clear history/roll count on next click after message.
 * 
 */

namespace Exercise06
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonRollDice = new System.Windows.Forms.Button();
            this.labelDie1 = new System.Windows.Forms.Label();
            this.labelDie2 = new System.Windows.Forms.Label();
            this.listBoxRollHistory = new System.Windows.Forms.ListBox();
            this.labelRollHistory = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonRollDice
            // 
            this.buttonRollDice.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRollDice.Location = new System.Drawing.Point(24, 28);
            this.buttonRollDice.Name = "buttonRollDice";
            this.buttonRollDice.Size = new System.Drawing.Size(234, 93);
            this.buttonRollDice.TabIndex = 0;
            this.buttonRollDice.Text = "Roll Dice";
            this.buttonRollDice.UseVisualStyleBackColor = true;
            this.buttonRollDice.Click += new System.EventHandler(this.buttonRollDice_Click);
            // 
            // labelDie1
            // 
            this.labelDie1.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelDie1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelDie1.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDie1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelDie1.Location = new System.Drawing.Point(24, 150);
            this.labelDie1.Name = "labelDie1";
            this.labelDie1.Size = new System.Drawing.Size(100, 100);
            this.labelDie1.TabIndex = 1;
            this.labelDie1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelDie2
            // 
            this.labelDie2.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelDie2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelDie2.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDie2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelDie2.Location = new System.Drawing.Point(158, 150);
            this.labelDie2.Name = "labelDie2";
            this.labelDie2.Size = new System.Drawing.Size(100, 100);
            this.labelDie2.TabIndex = 2;
            this.labelDie2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // listBoxRollHistory
            // 
            this.listBoxRollHistory.FormattingEnabled = true;
            this.listBoxRollHistory.Location = new System.Drawing.Point(298, 51);
            this.listBoxRollHistory.Name = "listBoxRollHistory";
            this.listBoxRollHistory.Size = new System.Drawing.Size(240, 199);
            this.listBoxRollHistory.TabIndex = 3;
            // 
            // labelRollHistory
            // 
            this.labelRollHistory.AutoSize = true;
            this.labelRollHistory.Location = new System.Drawing.Point(295, 28);
            this.labelRollHistory.Name = "labelRollHistory";
            this.labelRollHistory.Size = new System.Drawing.Size(60, 13);
            this.labelRollHistory.TabIndex = 4;
            this.labelRollHistory.Text = "Roll History";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 281);
            this.Controls.Add(this.labelRollHistory);
            this.Controls.Add(this.listBoxRollHistory);
            this.Controls.Add(this.labelDie2);
            this.Controls.Add(this.labelDie1);
            this.Controls.Add(this.buttonRollDice);
            this.Name = "Form1";
            this.Text = "Dice Roll";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRollDice;
        private System.Windows.Forms.Label labelDie1;
        private System.Windows.Forms.Label labelDie2;
        private System.Windows.Forms.ListBox listBoxRollHistory;
        private System.Windows.Forms.Label labelRollHistory;
    }
}


﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Dariche
 * Assignment: Project01
 * ---------------------------------------------------------------
 * Description:
 * Create a form that converts a unit of measure to another unit.
 * (i.e. Weight on Earth to Weight of Mars)
 * Round output to always 3 decimal places
 * Capture errors in try-catch clause and provide a message
 * 
 */


using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                double weightEarth = double.Parse(tb_weightEarth.Text);
                double weightMars = (weightEarth * 0.377);
                tb_weightMars.Text = weightMars.ToString("0.000");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Weight must be numeric.", "Earth Weight");
            }
        }
    }
}

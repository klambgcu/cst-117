﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Dariche
 * Assignment: Project01
 * ---------------------------------------------------------------
 * Description:
 * Create a form that converts a unit of measure to another unit.
 * (i.e. Weight on Earth to Weight of Mars)
 * Round output to always 3 decimal places
 * Capture errors in try-catch clause and provide a message
 * 
 */

namespace Project01
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_weightEarth = new System.Windows.Forms.Label();
            this.lbl_weightMars = new System.Windows.Forms.Label();
            this.tb_weightEarth = new System.Windows.Forms.TextBox();
            this.tb_weightMars = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_weightEarth
            // 
            this.lbl_weightEarth.AutoSize = true;
            this.lbl_weightEarth.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_weightEarth.Location = new System.Drawing.Point(21, 36);
            this.lbl_weightEarth.Name = "lbl_weightEarth";
            this.lbl_weightEarth.Size = new System.Drawing.Size(258, 22);
            this.lbl_weightEarth.TabIndex = 0;
            this.lbl_weightEarth.Text = "Enter your weight on Earth";
            // 
            // lbl_weightMars
            // 
            this.lbl_weightMars.AutoSize = true;
            this.lbl_weightMars.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_weightMars.Location = new System.Drawing.Point(78, 99);
            this.lbl_weightMars.Name = "lbl_weightMars";
            this.lbl_weightMars.Size = new System.Drawing.Size(201, 22);
            this.lbl_weightMars.TabIndex = 1;
            this.lbl_weightMars.Text = "Your weight on Mars";
            // 
            // tb_weightEarth
            // 
            this.tb_weightEarth.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_weightEarth.Location = new System.Drawing.Point(298, 29);
            this.tb_weightEarth.MaxLength = 10;
            this.tb_weightEarth.Name = "tb_weightEarth";
            this.tb_weightEarth.Size = new System.Drawing.Size(113, 29);
            this.tb_weightEarth.TabIndex = 1;
            // 
            // tb_weightMars
            // 
            this.tb_weightMars.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_weightMars.Location = new System.Drawing.Point(298, 92);
            this.tb_weightMars.MaxLength = 10;
            this.tb_weightMars.Name = "tb_weightMars";
            this.tb_weightMars.ReadOnly = true;
            this.tb_weightMars.Size = new System.Drawing.Size(113, 29);
            this.tb_weightMars.TabIndex = 2;
            this.tb_weightMars.TabStop = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.DeepSkyBlue;
            this.button1.FlatAppearance.BorderSize = 2;
            this.button1.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(311, 155);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 36);
            this.button1.TabIndex = 3;
            this.button1.Text = "Convert";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(443, 223);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.tb_weightMars);
            this.Controls.Add(this.tb_weightEarth);
            this.Controls.Add(this.lbl_weightMars);
            this.Controls.Add(this.lbl_weightEarth);
            this.Name = "Form1";
            this.Text = "Your Weight on Mars";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_weightEarth;
        private System.Windows.Forms.Label lbl_weightMars;
        private System.Windows.Forms.TextBox tb_weightEarth;
        private System.Windows.Forms.TextBox tb_weightMars;
        private System.Windows.Forms.Button button1;
    }
}


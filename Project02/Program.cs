﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 2
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a listbox, checkboxs, radiobuttons, labels
 * and buttons. It will also draw shapes on a panel with optional
 * fill or outline based upon radiobuttons and display name and color
 * depending upon checkbox selections.
 * 
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project02
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}

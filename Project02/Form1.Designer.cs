﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 2
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a listbox, checkboxs, radiobuttons, labels
 * and buttons. It will also draw shapes on a panel with optional
 * fill or outline based upon radiobuttons and display name and color
 * depending upon checkbox selections.
 * 
 */
 
namespace Project02
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxShapes = new System.Windows.Forms.ListBox();
            this.labelListBoxShapes = new System.Windows.Forms.Label();
            this.groupBoxFillMode = new System.Windows.Forms.GroupBox();
            this.radioButtonOutline = new System.Windows.Forms.RadioButton();
            this.radioButtonFill = new System.Windows.Forms.RadioButton();
            this.groupBoxDetails = new System.Windows.Forms.GroupBox();
            this.checkBoxColor = new System.Windows.Forms.CheckBox();
            this.checkBoxName = new System.Windows.Forms.CheckBox();
            this.buttonDrawShape = new System.Windows.Forms.Button();
            this.buttonClearShapeArea = new System.Windows.Forms.Button();
            this.panelShapeArea = new System.Windows.Forms.Panel();
            this.labelShapeName = new System.Windows.Forms.Label();
            this.labelShapeColor = new System.Windows.Forms.Label();
            this.groupBoxFillMode.SuspendLayout();
            this.groupBoxDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBoxShapes
            // 
            this.listBoxShapes.DisplayMember = "Circle";
            this.listBoxShapes.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listBoxShapes.FormattingEnabled = true;
            this.listBoxShapes.ItemHeight = 20;
            this.listBoxShapes.Items.AddRange(new object[] {
            "Circle",
            "Hexagon",
            "Pentagon",
            "Rectangle",
            "Triangle"});
            this.listBoxShapes.Location = new System.Drawing.Point(89, 69);
            this.listBoxShapes.Name = "listBoxShapes";
            this.listBoxShapes.Size = new System.Drawing.Size(200, 104);
            this.listBoxShapes.TabIndex = 0;
            this.listBoxShapes.SelectedIndexChanged += new System.EventHandler(this.listBoxShapes_SelectedIndexChanged);
            // 
            // labelListBoxShapes
            // 
            this.labelListBoxShapes.AutoSize = true;
            this.labelListBoxShapes.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelListBoxShapes.Location = new System.Drawing.Point(86, 49);
            this.labelListBoxShapes.Name = "labelListBoxShapes";
            this.labelListBoxShapes.Size = new System.Drawing.Size(102, 17);
            this.labelListBoxShapes.TabIndex = 1;
            this.labelListBoxShapes.Text = "Select a shape";
            // 
            // groupBoxFillMode
            // 
            this.groupBoxFillMode.Controls.Add(this.radioButtonOutline);
            this.groupBoxFillMode.Controls.Add(this.radioButtonFill);
            this.groupBoxFillMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxFillMode.Location = new System.Drawing.Point(89, 227);
            this.groupBoxFillMode.Name = "groupBoxFillMode";
            this.groupBoxFillMode.Size = new System.Drawing.Size(200, 100);
            this.groupBoxFillMode.TabIndex = 2;
            this.groupBoxFillMode.TabStop = false;
            this.groupBoxFillMode.Text = "Select fill mode";
            // 
            // radioButtonOutline
            // 
            this.radioButtonOutline.AutoSize = true;
            this.radioButtonOutline.Checked = true;
            this.radioButtonOutline.Location = new System.Drawing.Point(7, 63);
            this.radioButtonOutline.Name = "radioButtonOutline";
            this.radioButtonOutline.Size = new System.Drawing.Size(71, 21);
            this.radioButtonOutline.TabIndex = 1;
            this.radioButtonOutline.TabStop = true;
            this.radioButtonOutline.Text = "Outline";
            this.radioButtonOutline.UseVisualStyleBackColor = true;
            // 
            // radioButtonFill
            // 
            this.radioButtonFill.AutoSize = true;
            this.radioButtonFill.Location = new System.Drawing.Point(7, 30);
            this.radioButtonFill.Name = "radioButtonFill";
            this.radioButtonFill.Size = new System.Drawing.Size(43, 21);
            this.radioButtonFill.TabIndex = 0;
            this.radioButtonFill.Text = "Fill";
            this.radioButtonFill.UseVisualStyleBackColor = true;
            // 
            // groupBoxDetails
            // 
            this.groupBoxDetails.Controls.Add(this.checkBoxColor);
            this.groupBoxDetails.Controls.Add(this.checkBoxName);
            this.groupBoxDetails.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxDetails.Location = new System.Drawing.Point(89, 369);
            this.groupBoxDetails.Name = "groupBoxDetails";
            this.groupBoxDetails.Size = new System.Drawing.Size(200, 100);
            this.groupBoxDetails.TabIndex = 3;
            this.groupBoxDetails.TabStop = false;
            this.groupBoxDetails.Text = "Select details";
            // 
            // checkBoxColor
            // 
            this.checkBoxColor.AutoSize = true;
            this.checkBoxColor.Location = new System.Drawing.Point(20, 63);
            this.checkBoxColor.Name = "checkBoxColor";
            this.checkBoxColor.Size = new System.Drawing.Size(60, 21);
            this.checkBoxColor.TabIndex = 1;
            this.checkBoxColor.Text = "Color";
            this.checkBoxColor.UseVisualStyleBackColor = true;
            this.checkBoxColor.CheckedChanged += new System.EventHandler(this.checkBoxColor_CheckedChanged);
            // 
            // checkBoxName
            // 
            this.checkBoxName.AutoSize = true;
            this.checkBoxName.Location = new System.Drawing.Point(20, 30);
            this.checkBoxName.Name = "checkBoxName";
            this.checkBoxName.Size = new System.Drawing.Size(64, 21);
            this.checkBoxName.TabIndex = 0;
            this.checkBoxName.Text = "Name";
            this.checkBoxName.UseVisualStyleBackColor = true;
            this.checkBoxName.CheckedChanged += new System.EventHandler(this.checkBoxName_CheckedChanged);
            // 
            // buttonDrawShape
            // 
            this.buttonDrawShape.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.buttonDrawShape.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.buttonDrawShape.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDrawShape.Location = new System.Drawing.Point(382, 559);
            this.buttonDrawShape.Name = "buttonDrawShape";
            this.buttonDrawShape.Size = new System.Drawing.Size(182, 30);
            this.buttonDrawShape.TabIndex = 4;
            this.buttonDrawShape.Text = "Draw";
            this.buttonDrawShape.UseVisualStyleBackColor = false;
            this.buttonDrawShape.Click += new System.EventHandler(this.buttonDrawShape_Click);
            // 
            // buttonClearShapeArea
            // 
            this.buttonClearShapeArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonClearShapeArea.Location = new System.Drawing.Point(600, 559);
            this.buttonClearShapeArea.Name = "buttonClearShapeArea";
            this.buttonClearShapeArea.Size = new System.Drawing.Size(182, 30);
            this.buttonClearShapeArea.TabIndex = 5;
            this.buttonClearShapeArea.Text = "Clear";
            this.buttonClearShapeArea.UseVisualStyleBackColor = true;
            this.buttonClearShapeArea.Click += new System.EventHandler(this.buttonClearShapeArea_Click);
            // 
            // panelShapeArea
            // 
            this.panelShapeArea.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panelShapeArea.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.panelShapeArea.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelShapeArea.Location = new System.Drawing.Point(382, 69);
            this.panelShapeArea.Name = "panelShapeArea";
            this.panelShapeArea.Size = new System.Drawing.Size(400, 400);
            this.panelShapeArea.TabIndex = 6;
            this.panelShapeArea.Paint += new System.Windows.Forms.PaintEventHandler(this.panelShapeArea_Paint);
            // 
            // labelShapeName
            // 
            this.labelShapeName.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShapeName.Location = new System.Drawing.Point(382, 482);
            this.labelShapeName.Name = "labelShapeName";
            this.labelShapeName.Size = new System.Drawing.Size(400, 23);
            this.labelShapeName.TabIndex = 7;
            this.labelShapeName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelShapeColor
            // 
            this.labelShapeColor.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelShapeColor.Location = new System.Drawing.Point(382, 518);
            this.labelShapeColor.Name = "labelShapeColor";
            this.labelShapeColor.Size = new System.Drawing.Size(400, 23);
            this.labelShapeColor.TabIndex = 8;
            this.labelShapeColor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(868, 639);
            this.Controls.Add(this.labelShapeColor);
            this.Controls.Add(this.labelShapeName);
            this.Controls.Add(this.panelShapeArea);
            this.Controls.Add(this.buttonDrawShape);
            this.Controls.Add(this.buttonClearShapeArea);
            this.Controls.Add(this.groupBoxDetails);
            this.Controls.Add(this.groupBoxFillMode);
            this.Controls.Add(this.labelListBoxShapes);
            this.Controls.Add(this.listBoxShapes);
            this.Name = "Form1";
            this.Text = "Shape Maker";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBoxFillMode.ResumeLayout(false);
            this.groupBoxFillMode.PerformLayout();
            this.groupBoxDetails.ResumeLayout(false);
            this.groupBoxDetails.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxShapes;
        private System.Windows.Forms.Label labelListBoxShapes;
        private System.Windows.Forms.GroupBox groupBoxFillMode;
        private System.Windows.Forms.RadioButton radioButtonOutline;
        private System.Windows.Forms.RadioButton radioButtonFill;
        private System.Windows.Forms.GroupBox groupBoxDetails;
        private System.Windows.Forms.CheckBox checkBoxColor;
        private System.Windows.Forms.CheckBox checkBoxName;
        private System.Windows.Forms.Button buttonDrawShape;
        private System.Windows.Forms.Button buttonClearShapeArea;
        private System.Windows.Forms.Panel panelShapeArea;
        private System.Windows.Forms.Label labelShapeName;
        private System.Windows.Forms.Label labelShapeColor;
    }
}


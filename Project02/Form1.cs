﻿/*
 * Name: Kelly Lamb
 * Class: CST-117 Enterprise Computer Promgramming I
 * Professor: Dr. Aiman Darwiche
 * Assignment: Project 2
 * ---------------------------------------------------------------
 * Description:
 * Create a form that uses a listbox, checkboxs, radiobuttons, labels
 * and buttons. It will also draw shapes on a panel with optional
 * fill or outline based upon radiobuttons and display name and color
 * depending upon checkbox selections.
 * 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Project02
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            listBoxShapes.SelectedIndex = -1;
        }

        private void checkBoxName_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxName.Checked)
                if (listBoxShapes.SelectedIndex >= 0)
                    labelShapeName.Text = listBoxShapes.Items[listBoxShapes.SelectedIndex].ToString();
                else
                    labelShapeName.Text = "";
            else
                labelShapeName.Text = "";
        }

        private void checkBoxColor_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxColor.Checked)
            {
                String[] colors = { "Red", "Purple", "Blue", "Green", "Yellow" };
                if (listBoxShapes.SelectedIndex >= 0)
                    labelShapeColor.Text = colors[listBoxShapes.SelectedIndex];
                else
                    labelShapeColor.Text = "";
            }
            else
                labelShapeColor.Text = "";
        }

        private void listBoxShapes_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkBoxName_CheckedChanged(sender, e);
            checkBoxColor_CheckedChanged(sender, e);
        }

        private void buttonDrawShape_Click(object sender, EventArgs e)
        {
             panelShapeArea.Invalidate();
        }

        private void buttonClearShapeArea_Click(object sender, EventArgs e)
        {
            listBoxShapes.SelectedIndex = -1;
            panelShapeArea.Invalidate();
        }

        private void panelShapeArea_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = panelShapeArea.CreateGraphics();

            // Draw a circle
            if (listBoxShapes.SelectedIndex == 0)
            {
                SolidBrush sb = new SolidBrush(Color.Red);
                Pen p = new Pen(Color.Red);
                p.Width = 10;
                g.DrawEllipse(p, 20, 20, panelShapeArea.Width - 40, panelShapeArea.Height - 40);
                if (radioButtonFill.Checked)
                    g.FillEllipse(sb, 20, 20, panelShapeArea.Width - 40, panelShapeArea.Height - 40);

            }

            // Draw a hexagon
            if (listBoxShapes.SelectedIndex == 1)
            {
                SolidBrush sb = new SolidBrush(Color.DarkViolet);
                Pen p = new Pen(Color.DarkViolet);
                p.Width = 10;

                Point center = new Point(panelShapeArea.Width / 2, panelShapeArea.Height / 2);
                // Create points that define polygon.
                Point[] curvePoints = CalculateVertices(6, ((panelShapeArea.Height - 20) / 2), 0, center);
                g.DrawPolygon(p, curvePoints);
                if (radioButtonFill.Checked)
                    g.FillPolygon(sb, curvePoints);

            }

            // Draw a pentagon
            if (listBoxShapes.SelectedIndex == 2)
            {
                SolidBrush sb = new SolidBrush(Color.Blue);
                Pen p = new Pen(Color.Blue);
                p.Width = 10;

                Point center = new Point(panelShapeArea.Width / 2, panelShapeArea.Height / 2 + 20);
                // Create points that define polygon.
                Point[] curvePoints = CalculateVertices(5, ((panelShapeArea.Height-20) / 2), 18, center);
                g.DrawPolygon(p, curvePoints);
                if (radioButtonFill.Checked)
                    g.FillPolygon(sb, curvePoints);

            }

            // Draw a rectangle
            if (listBoxShapes.SelectedIndex == 3)
            {
                SolidBrush sb = new SolidBrush(Color.Green);
                Pen p = new Pen(Color.Green);
                p.Width = 10;
                g.DrawRectangle(p, 20, 20, panelShapeArea.Width - 40, panelShapeArea.Height - 40);
                if (radioButtonFill.Checked)
                    g.FillRectangle(sb, 20, 20, panelShapeArea.Width - 40, panelShapeArea.Height - 40);
            }

            // Draw a triangle
            if (listBoxShapes.SelectedIndex== 4)
            {
                SolidBrush sb = new SolidBrush(Color.Yellow);
                Pen p = new Pen(Color.Yellow);
                p.Width = 10;

                // Create points that define polygon.
                Point point1 = new Point(panelShapeArea.Width / 2, 20);
                Point point2 = new Point(20, panelShapeArea.Height - 20);
                Point point3 = new Point(panelShapeArea.Width - 20, panelShapeArea.Height - 20);
                Point[] curvePoints = { point1, point2, point3 };

                g.DrawPolygon(p, curvePoints);
                if (radioButtonFill.Checked)
                    g.FillPolygon(sb, curvePoints);

            }
        }
        private Point[] CalculateVertices(int sides, int radius, int startingAngle, Point center)
        {
            if (sides < 3)
                throw new ArgumentException("Polygon must have 3 sides or more.");

            List<Point> points = new List<Point>();
            float step = 360.0f / sides;

            float angle = startingAngle; //starting angle
            for (double i = startingAngle; i < startingAngle + 360.0; i += step) //go in a full circle
            {
                points.Add(DegreesToXY(angle, radius, center)); //code snippet from above
                angle += step;
            }

            return points.ToArray();
        }

        private Point DegreesToXY(float degrees, float radius, Point origin)
        {
            Point xy = new Point();
            double radians = degrees * Math.PI / 180.0;

            xy.X = (int)Math.Round((float)Math.Cos(radians) * radius + origin.X);
            xy.Y = (int)Math.Round((float)Math.Sin(-radians) * radius + origin.Y);

            return xy;
        }

    }
}
